package com.imm.androidsimulasi;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.imm.androidsimulasi.register.RegisterActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;


/**
 * Created by lembah8 on 11/30/16.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class RegisterScreenTest {
    private String fullname = "Rahmat Zulfikri";
    private String email = "zul@gmail.com";
    private String password = "password";

    /**
     * Merupakan fungsi yang digunakan untuk memeriksa apakah error text yang dikeluarkan sesuai
     * dengan error text yang seharusnya.
     * @param expectedErrorText
     * @return
     */
    public static Matcher<View> hasErrorInputText(final String expectedErrorText) {
        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {

            }

            @Override
            protected boolean matchesSafely(View item) {
                if (!(item instanceof EditText)) {
                    return false;
                }

                CharSequence error = ((EditText) item).getError();
                if (error == null) {
                    return false;
                }

                String errorText = error.toString();
                Log.d("error", errorText);
                return expectedErrorText.equals(errorText);
            }
        };
    }

    @Rule
    public ActivityTestRule<RegisterActivity> registerActivityActivityTestRule = new ActivityTestRule<RegisterActivity>(RegisterActivity.class);


    /**
     * Skema error input UI
     * Error text pada input akan muncul dari input yang paling atas.
     * - Semua field kosong : maka aka muncul error pada input field paling atas (fullname)
     * - Apabila ada dua field kosong maka urutan field paling awal yang akan memunculkan error.
     */


    /**
     * Digunakan untuk memerika kemunculan error required field pada field fullname
     * dengan kondisi field fullname kosong
     * @throws Exception
     */
    @Test
    public void check_required_field_on_fullname() throws Exception{
        onView(withId(R.id.btn_register)).perform(click());
        onView(withId(R.id.edt_fullname)).check(matches(hasErrorInputText(registerActivityActivityTestRule.getActivity().getString(R.string.error_field_required))));
    }

    /**
     * Digunakan untuk memeriksa kemunculan error required field pada field email
     * dengan kondisi field yang berada diatas field email harus terisi (Fullname)
     * @throws Exception
     */
    @Test
    public void check_required_field_on_email() throws Exception{
        onView(withId(R.id.edt_fullname)).perform(typeText(fullname), closeSoftKeyboard());
        onView(withId(R.id.btn_register)).perform(click());
        onView(withId(R.id.edt_email)).check(matches(hasErrorInputText(registerActivityActivityTestRule.getActivity().getString(R.string.error_field_required))));
    }

    /**
     * Digunakan untuk memeriksa kemunculan error required field pada field password
     * dengan kodisi field yang berada diatas field password harus terisi (Fullname, Email)
     * @throws Exception
     */
    @Test
    public void check_required_field_on_password() throws Exception{
        onView(withId(R.id.edt_fullname)).perform(typeText(fullname), closeSoftKeyboard());
        onView(withId(R.id.edt_email)).perform(typeText(email), closeSoftKeyboard());
        onView(withId(R.id.btn_register)).perform(click());
        onView(withId(R.id.edt_password)).check(matches(hasErrorInputText(registerActivityActivityTestRule.getActivity().getString(R.string.error_field_required))));

    }

    /**
     * Digunakan untuk memeriksa kemunculan error required field pada field rePassword
     * dengan kondisi field yang berada diatas field rePassword harus terisi (Fullname, Email, Password)
     * @throws Exception
     */
    @Test
    public void check_required_field_on_repassword() throws Exception{
        onView(withId(R.id.edt_fullname)).perform(typeText(fullname), closeSoftKeyboard());
        onView(withId(R.id.edt_email)).perform(typeText(email), closeSoftKeyboard());
        onView(withId(R.id.edt_password)).perform(typeText(password), closeSoftKeyboard());
        onView(withId(R.id.btn_register)).perform(click());
        onView(withId(R.id.edt_rePassword)).check(matches(hasErrorInputText(registerActivityActivityTestRule.getActivity().getString(R.string.error_field_required))));
    }

    /**
     * Digunakan untuk memeriksa kemunculan error pada field email apabila. email yang diinputkan tidak valid
     * dengan kondisi email tidak valid yaitu mengandung simbol "@" (at) dan " " (space)
     * @throws Exception
     */
    @Test
    public void check_invalid_email() throws Exception {
        onView(withId(R.id.edt_fullname)).perform(typeText(fullname), closeSoftKeyboard());
        onView(withId(R.id.edt_email)).perform(typeText(fullname), closeSoftKeyboard());
        onView(withId(R.id.edt_password)).perform(typeText(password), closeSoftKeyboard());
        onView(withId(R.id.edt_rePassword)).perform(typeText(password), closeSoftKeyboard());
        onView(withId(R.id.btn_register)).perform(click());

        onView(withId(R.id.edt_email)).check(matches(hasErrorInputText(registerActivityActivityTestRule.getActivity().getString(R.string.error_invalid_email))));
    }

    /**
     * Digunakan untuk memeriksa kemunculan error pada field password. apabila password yang diinputkan tidak memenuhi kriteria
     * dengan kondisi kriteria yang tepenuhi yaitu password > 7 karakter
     * @throws Exception
     */
    @Test
    public void check_short_password() throws Exception {
        onView(withId(R.id.edt_fullname)).perform(typeText(fullname), closeSoftKeyboard());
        onView(withId(R.id.edt_email)).perform(typeText(email), closeSoftKeyboard());
        onView(withId(R.id.edt_password)).perform(typeText("123456"), closeSoftKeyboard());
        onView(withId(R.id.edt_rePassword)).perform(typeText("123456"), closeSoftKeyboard());
        onView(withId(R.id.btn_register)).perform(click());

        onView(withId(R.id.edt_password)).check(matches(hasErrorInputText(registerActivityActivityTestRule.getActivity().getString(R.string.error_register_short_password))));
    }

    /**
     * Digunakan untuk memeriksa kemunculan error pada field rePassword.
     * apabila password yang diinputkan dengan rePassword yang diinputkan tidak sesuai
     * @throws Exception
     */
    @Test
    public void check_password_doestMatch() throws Exception {
        onView(withId(R.id.edt_fullname)).perform(typeText(fullname), closeSoftKeyboard());
        onView(withId(R.id.edt_email)).perform(typeText(email), closeSoftKeyboard());
        onView(withId(R.id.edt_password)).perform(typeText(password), closeSoftKeyboard());
        onView(withId(R.id.edt_rePassword)).perform(typeText(password + "123"), closeSoftKeyboard());
        onView(withId(R.id.btn_register)).perform(click());

        onView(withId(R.id.edt_rePassword)).check(matches(hasErrorInputText(registerActivityActivityTestRule.getActivity().getString(R.string.error_password_not_same))));
    }

//    @Test
    public void check_register() throws  Exception{
        onView(withId(R.id.edt_fullname)).perform(typeText(fullname), closeSoftKeyboard());
        onView(withId(R.id.edt_email)).perform(typeText(email), closeSoftKeyboard());
        onView(withId(R.id.edt_password)).perform(typeText(password), closeSoftKeyboard());
        onView(withId(R.id.edt_rePassword)).perform(typeText(password), closeSoftKeyboard());
        onView(withId(R.id.btn_register)).perform(click());
    }

}
