package com.imm.androidsimulasi.UserActionVideo;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.imm.androidsimulasi.R;


public class UserActionVideoFragment extends Fragment implements UserActionVideoContract.view{

    ImageView imgUserActionVideo;
    String userId;
    String userVideoId;
    String token;
    String videoId;
    Context context;

    private UserActionVideoContract.actionListener userActionVideoListener;

    public UserActionVideoFragment(String videoId, String userId, String userVideoId, String token, Context context) {
        this.videoId = videoId;
        this.userId = userId;
        this.userVideoId = userVideoId;
        this.context = context;
        this.userActionVideoListener = new UserActionVideoPresenter(this);
        this.token = token;
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_user_action_video, container, false);
        imgUserActionVideo = (ImageView) rootView.findViewById(R.id.img_detail_option_video);

        imgUserActionVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context, imgUserActionVideo);
                if (userId.equals(userVideoId)) {
                    popupMenu.getMenuInflater().inflate(R.menu.menu_user_video_option, popupMenu.getMenu());
                } else {
                    popupMenu.getMenuInflater().inflate(R.menu.menu_user_general_option, popupMenu.getMenu());
                }
                popupMenu.setGravity(Gravity.BOTTOM);

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().equals(getResources().getString(R.string.option_delete))) {
                            deleteVideoDialog();
                        }
                        if (item.getTitle().equals(getResources().getString(R.string.option_report))) {
                            System.out.println("report boss");
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void deleteVideoDialog(){
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("Delete Video");
        alert.setMessage("did you want to delete this video ?");
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                userActionVideoListener.doDeleteVideo(videoId, token);
//                videoListener.deleteVideo();
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        alert.show();
    }

    @Override
    public void toastMessage(String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public Context getContext(){
        return context;
    }

    public interface OnFragmentInteractionListener {
    }
}
