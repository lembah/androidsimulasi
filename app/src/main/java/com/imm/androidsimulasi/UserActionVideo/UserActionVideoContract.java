package com.imm.androidsimulasi.UserActionVideo;

import android.content.Context;

/**
 * Created by lembah8 on 12/6/16.
 */

public interface UserActionVideoContract {
    interface view{
        void toastMessage(String message);
        Context getContext();
    }

    interface actionListener{
        void doDeleteVideo(String videoId, String token);
        void doReportVideo();
        void sucessResponse(String message);
        void failedResponse(String message);
    }
}
