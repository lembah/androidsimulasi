package com.imm.androidsimulasi.UserActionVideo;

import com.imm.androidsimulasi.Data.VideoServiceApi;
import com.imm.androidsimulasi.Data.VideoServiceApiImpl;
import com.imm.androidsimulasi.activity.MainActivity;
import com.imm.androidsimulasi.helper.IntentHelper;

/**
 * Created by lembah8 on 12/6/16.
 */

public class UserActionVideoPresenter implements UserActionVideoContract.actionListener{

    private final UserActionVideoContract.view userActionVideoView;
//    private final VideoServiceApi.VideoServiceCallback callback;
    VideoServiceApi videoServiceApi;

    public UserActionVideoPresenter(UserActionVideoContract.view userActionView){
        this.userActionVideoView = userActionView;
        this.videoServiceApi = new VideoServiceApiImpl();
    }

    @Override
    public void doDeleteVideo(String videoId, String token) {
        videoServiceApi.deleteVideo(videoId, token, new VideoServiceApi.VideoServiceCallback<String>() {
            @Override
            public void onLoaded(String message) {
                if(message.equals("true"))
                {
                    userActionVideoView.toastMessage("Video Deleted");
                    IntentHelper.backActivity(userActionVideoView.getContext(), MainActivity.class);
                }else if(message.equals("false")){
                    userActionVideoView.toastMessage(message);
                }
            }
        });
    }

    @Override
    public void doReportVideo() {

    }

    @Override
    public void sucessResponse(String message) {

    }

    @Override
    public void failedResponse(String message) {

    }

}
