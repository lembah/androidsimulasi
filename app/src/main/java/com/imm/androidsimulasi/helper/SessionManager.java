package com.imm.androidsimulasi.helper;

/**
 * Created by lembah7 on 11/14/16.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.imm.androidsimulasi.activity.LoginActivity;
import com.imm.androidsimulasi.activity.WelcomeActivity;

import java.util.HashMap;

import java.util.HashMap;

public class SessionManager {
        // LogCat tag
        private static String TAG = SessionManager.class.getSimpleName();

        // Shared Preferences
        SharedPreferences pref;

        Editor editor;
        Context _context;

        // Shared pref mode
        int PRIVATE_MODE = 0;

        // Shared preferences file name
        private static final String PREF_NAME = "SimulasiLogin";

        private static final String KEY_IS_LOGGEDIN = "isLoggedIn";

        private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

        // Public Variable
        public static final String KEY_ID       = "sim_id";
        public static final String KEY_NAME     = "sim_name";
        public static final String KEY_EMAIL    = "sim_email";
        public static final String KEY_TOKEN    = "sim_token";

        public SessionManager(Context context) {
            this._context = context;
            pref    = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
            editor  = pref.edit();
        }

        public void setLogin(boolean isLoggedIn) {

            editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);
            // commit changes
            editor.commit();


            Log.d(TAG, "User login session modified!");
        }

        /**
         * Create login session
         * */
        public void createLoginSession(String id, String name, String email, String token){
            // Storing login value as TRUE
            editor.putBoolean(KEY_IS_LOGGEDIN, true);

            // Storing name in pref
            editor.putString(KEY_ID, id);
            editor.putString(KEY_NAME, name);
            editor.putString(KEY_EMAIL, email);
            editor.putString(KEY_TOKEN, token);

            // commit changes
            editor.commit();
        }

        /**
         * Clear session details
         * */
        public void logoutUser(){
            // Clearing all data from Shared Preferences
            editor.clear();
            editor.commit();

            IntentHelper.redirectActivity(_context, WelcomeActivity.class);

        }

        public boolean isLoggedIn(){
            return pref.getBoolean(KEY_IS_LOGGEDIN, false);
        }

        public void setFirstTimeLaunch(boolean isFirstTime) {
            editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
            editor.commit();
        }

        public boolean isFirstTimeLaunch() {
            return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
        }


        /**
         * Get stored session data
         * */
        public HashMap<String, String> getUserDetails(){
            HashMap<String, String> user = new HashMap<String, String>();
            // put attributes
            user.put(KEY_ID, pref.getString(KEY_ID, null));
            user.put(KEY_NAME, pref.getString(KEY_NAME, null));
            user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
            user.put(KEY_TOKEN, pref.getString(KEY_TOKEN, null));

            // return user
            return user;
        }

        public static String getKeyToken(Context context){
            // Get user data from session
            final SessionManager session = new SessionManager(context.getApplicationContext());
            HashMap<String, String> user = session.getUserDetails();
            String token = user.get(SessionManager.KEY_TOKEN);

            return token;
        }

        public static String getUserId(Context context){
            // Get user data from session
            final SessionManager session = new SessionManager(context.getApplicationContext());
            HashMap<String, String> user = session.getUserDetails();
            String id = user.get(SessionManager.KEY_ID);

        return id;
    }

}
