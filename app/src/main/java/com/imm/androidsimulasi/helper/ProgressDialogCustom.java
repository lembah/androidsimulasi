package com.imm.androidsimulasi.helper;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by lembah7 on 11/14/16.
 */

public class ProgressDialogCustom {
    // Progress dialog
    private ProgressDialog pDialog;

    public ProgressDialogCustom(Context context){
        this.pDialog = new ProgressDialog(context);
    }

    public void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    public void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
