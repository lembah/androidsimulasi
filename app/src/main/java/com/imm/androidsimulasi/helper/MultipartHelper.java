package com.imm.androidsimulasi.helper;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Created by lembah8 on 11/17/16.
 *
 * MultipartHelper merupakan helper untuk melakukan pengiriman data berupa multipart/form-data
 * cara penggunaan helperini yaitu :
 * - Inisialisasi
 * - penambahan header (Optional)
 * - Mulai helper
 * - Penambahan parameter
 * - Penambahan file
 * - Penutup helper
 *
 * contoh :
 * MultipartHelper multipart = new MultipartHelper(requestURL);  // inisialisasi
 * multipart.addHeaderField("token", token);                     // penambahan header
 * multipart.start();                                            // memulai helper
 * multipart.addFormField("nama_field", "value_field");          // penambahan parameter
 * multipart.addFilePart("nama_field", "path_video");            // penambahan file
 * String response = multipart.finish();                         // penutup helper
 *
 */

public class MultipartHelper {

    HttpURLConnection conn = null;
    DataOutputStream dos = null;
    String lineEnd = "\r\n";
    String twoHyphens = "--";
    String boundary = "*****";
    int bytesRead, bytesAvailable, bufferSize;
    byte[] buffer;
    int maxBufferSize = 1 * 1024 * 1024;


    /**
     * Merupakan fungsi untuk inisialisasi helper dengan parameter :
     * requestURL = berisi String url API yang dituju
     *
     * @param requestURL
     * @throws IOException
     */
    public MultipartHelper(String requestURL) throws IOException {
        URL url = new URL(requestURL);
        conn = (HttpURLConnection) url.openConnection();
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setUseCaches(false);
//        conn.setRequestMethod("POST");
        conn.setRequestProperty("Connection", "Keep-Alive");
        conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
    }

    /**
     * merupakan fungsi untuk memberikan header request method dengan parameter
     * - POST
     * - PUT
     * - GET
     * - DELETE
     * @param method
     * @throws ProtocolException
     */
    public void addRequestMehod(String method) throws ProtocolException {
        conn.setRequestMethod(method);
    }

    /**
     * Merupakan fungsi untuk menambahkan header pada request dengan parameter :
     * name     : merupakan nama field/content header
     * value    : merupakan value dari field/content header yang ditambahkan
     *
     * @param name
     * @param value
     * @throws IOException
     */
    public void addHeaderField(String name, String value) throws IOException {
        conn.setRequestProperty(name, value);
    }

    /**
     * Merupakan fungsi untuk memulai helper multipart/form-data menggunakan dataOutputStream
     *
     * @throws IOException
     */
    public void start() throws IOException {
        dos = new DataOutputStream(conn.getOutputStream());
    }

    /**
     * Merupakan fungsi untuk menambahkan parameter/informasi yang ingin dikirimkan berbarengan dengan file,
     * dengan parameter yaitu :
     * - name   = merupakan nama dari field yang ingin ditambahkan
     * - value  = merupakan value dari field yang ingin ditambahkan
     *
     * @param name
     * @param value
     * @throws IOException
     */
    public void addFormField(String name, String value) throws IOException {
        dos.writeBytes(twoHyphens + boundary + lineEnd);
        dos.writeBytes("Content-Disposition: form-data; name=\""+name+"\"" + lineEnd);
        dos.writeBytes(lineEnd);

        dos.writeBytes(value);
        dos.writeBytes(lineEnd);
        dos.writeBytes(twoHyphens + boundary + lineEnd);
        dos.flush();
    }

    /**
     * Merupakan fungsi untuk menambahkan file yang ingin dikirim, dengan parameter :
     * name     : merupakan nama/key dari field yang ingin dikirim
     * filePath : merupakan path dari file yang akan dikirim
     * ---------
     * Note : nama dari file yang dikirim sama dengan path yang tersedia
     * @param name
     * @param filePath
     * @throws IOException
     */
    public void addFilePart(String name, String filePath) throws IOException {
        String filename=filePath.substring(filePath.lastIndexOf("/")+1);
        System.out.println(filename);
        File sourceFile = new File(filePath);
        dos.writeBytes(twoHyphens + boundary + lineEnd);
        dos.writeBytes("Content-Disposition: form-data; name=\""+name+"\";filename=\"" + filename + "\"" + lineEnd);
        dos.writeBytes(lineEnd);

        FileInputStream fileInputStream = new FileInputStream(sourceFile);

        bytesAvailable = fileInputStream.available();

        bufferSize = Math.min(bytesAvailable, maxBufferSize);
        buffer = new byte[bufferSize];

        bytesRead = fileInputStream.read(buffer, 0, bufferSize);

        while (bytesRead > 0) {
            dos.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }

        dos.writeBytes(lineEnd);
        dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
        fileInputStream.close();
        dos.flush();
    }


    public String finish() throws IOException {
        dos.close();
        String data = "";
        int status = conn.getResponseCode();
        System.out.println(status);
        if (status == HttpURLConnection.HTTP_OK) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null) {
                data = data + line;
            }
            reader.close();
            conn.disconnect();
        } else {
            throw new IOException("Server returned non-OK status: " + status);
        }
        return data.isEmpty() ? null : data;
    }
}
