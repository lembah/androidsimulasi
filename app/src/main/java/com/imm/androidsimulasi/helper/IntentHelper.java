package com.imm.androidsimulasi.helper;

/**
 * Created by lembah7 on 11/15/16.
 */

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.imm.androidsimulasi.model.IntentExtra;

import java.util.ArrayList;

/**
 * Created by yusufinthehouse on 6/18/16.
 */
public class IntentHelper {
    public static void redirectActivity(Context currentContext, Class nextActivity) {
        Intent i = new Intent(currentContext, nextActivity);

        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        currentContext.startActivity(i);
    }

    public static void redirectActivityWithExtras(Context currentContext, Class nextActivity, ArrayList<IntentExtra> extras) {
        Intent i = new Intent(currentContext, nextActivity);

        for (IntentExtra extra: extras) {
            Log.w("test", extra.getValue());
            i.putExtra(extra.getKey(), extra.getValue());
        }

        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        currentContext.startActivity(i);
    }

    public static void backActivity(Context currentContext, Class nextActivity) {
        Intent i = new Intent(currentContext, nextActivity);

        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        currentContext.startActivity(i);
    }
}
