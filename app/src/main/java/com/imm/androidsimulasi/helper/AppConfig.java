package com.imm.androidsimulasi.helper;

public class AppConfig {
    public static final String HOST_URL = "http://dummyapi.getsandbox.com/";
    public static final String SIMULASI_URL = "http://simulasi.lembah.org/api/";

    // Server user login url
    public static String URL_LOGIN = SIMULASI_URL + "login";

    // Server timeline
    public static String URL_TIMELINE = SIMULASI_URL + "users/timeline";

    // Server user register url
    public static String URL_REGISTER = SIMULASI_URL + "signup";

    // Server upload video
    public static String URL_UPLOAD = SIMULASI_URL + "app/video";

    // Server get topic
    public static String URL_TOPIC = SIMULASI_URL + "app/supply/topic";

    // Servert get video topic
    public static String URL_VIDEO_TOPIC(String topicId) {
        return SIMULASI_URL + "topics/" + topicId + "/videos";
    }

    // Send Comment
    public static String URL_SEND_COMMENT(String videoID) {
        return SIMULASI_URL + "videos/" + videoID + "/add-comment";
    }

    // View Comment
    public static String URL_VIEW_COMMENT(String videoID) {
        return SIMULASI_URL + "videos/" + videoID + "/comments";
    }

    // Update Comment
    public static String URL_EDIT_COMMENT = SIMULASI_URL + "comments/";

    // Delete Comment
    public static String URL_DELETE_COMMENT = SIMULASI_URL + "comments/";

    // Delete Video
    public static String URL_DELETE_VIDEO = SIMULASI_URL + "videos/";

    // Like Video
    public static String URL_LIKE_VIDEO(String videoID) {
        return SIMULASI_URL + "videos/" + videoID + "/like";
    }

    // Dislike video
    public static String URL_DISLIKE_VIDEO(String videoID) {
        return SIMULASI_URL + "videos/" + videoID + "/unlike";
    }

    // View Profile
    public static String URL_VIEW_PROFILE(String userId) {
        return SIMULASI_URL + "users/" + userId + "/profile";
    }

    // View Update Profile
    public static String URL_VIEW_UPDATE_PROFILE(String userId) {
        return SIMULASI_URL + "users/" + userId + "/profile";
    }

    // Update Profile Pic
    public static String URL_UPDATE_PROFILE_PIC = SIMULASI_URL + "users-profile-picture";
}