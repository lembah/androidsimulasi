package com.imm.androidsimulasi.Data;

/**
 * Created by lembah8 on 12/6/16.
 */

public interface LikeDislikeServideApi {
    interface LikeDislikeServiceCallback<T> {

        void onLoaded(T message);
    }

    void setLike(String videoId, final String token, final LikeDislikeServiceCallback<String> callback);
    void setDislike(String videoId, final String token, final LikeDislikeServiceCallback<String> callback);
}
