package com.imm.androidsimulasi.Data;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.imm.androidsimulasi.activity.MainActivity;
import com.imm.androidsimulasi.activity.UploadVideoActivity;
import com.imm.androidsimulasi.helper.AppConfig;
import com.imm.androidsimulasi.helper.IntentHelper;
import com.imm.androidsimulasi.helper.MultipartHelper;
import com.imm.androidsimulasi.model.UpdateProfileModel;
import com.imm.androidsimulasi.util.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lembah8 on 12/8/16.
 */

public class ProfileServiceApiImpl implements ProfileServiceApi {
    @Override
    public void UpdateProfile(final String fullname, final String userId, final String token, final String userAgent, final ProfileServiceApi.ProfileServiceCallback<String> callbackMessage) {
        StringRequest strReq = new StringRequest(Request.Method.PUT,
                AppConfig.URL_VIEW_UPDATE_PROFILE(userId), new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    System.out.println(response.toString());
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        String message = jObj.getString("message");
                        callbackMessage.onLoaded(message);
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("message");
                        callbackMessage.onLoaded(errorMsg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    callbackMessage.onLoaded(e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callbackMessage.onLoaded(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<>();
                params.put("name", fullname);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("token", token);
                params.put("User-Agent", userAgent);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq);
    }

    @Override
    public void viewProfile(final String userId, final String token, final String userAgent, final ProfileServiceCallback<UpdateProfileModel> callback, final ProfileServiceCallback<String> callbackMessage) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                AppConfig.URL_VIEW_PROFILE(userId), null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    System.out.println(response.toString());
                    boolean status = response.getBoolean("error");
                    Log.e("status", String.valueOf(status));

                    String fullname = response.getString("name");
                    String profilPic = response.getString("profile_picture");

                    UpdateProfileModel updateProfileModel = new UpdateProfileModel();
                    updateProfileModel.setUserId(userId);
                    updateProfileModel.setUserFullname(fullname);
                    updateProfileModel.setUserPic(profilPic);

                    if (updateProfileModel == null) {
                        callback.onLoaded(null);
                        callbackMessage.onLoaded("false");
                    } else if (updateProfileModel != null) {
                        callback.onLoaded(updateProfileModel);
                        callbackMessage.onLoaded("true");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onLoaded(null);
                    callbackMessage.onLoaded(e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onLoaded(null);
                callbackMessage.onLoaded(error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("token", token);
                params.put("User-Agent", userAgent);
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    @Override
    public void updateProfilPic(final Uri imageUri, final String token, final String userAgent, final ProfileServiceApi.ProfileServiceCallback<List<String>> callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String url = AppConfig.URL_UPDATE_PROFILE_PIC;
                String filePath = imageUri.getPath();
                List<String> responseImage = new ArrayList<String>();
                try {
                    MultipartHelper multipart = new MultipartHelper(url);
                    multipart.addRequestMehod("PUT");
                    multipart.addHeaderField("token", token);
                    multipart.addHeaderField("User-Agent", userAgent);
                    multipart.start();
                    multipart.addFilePart("avatar", filePath);
                    String response = multipart.finish();
                    Log.d("SERVER REPLIED", response);

                    JSONObject jsonObject = new JSONObject(response);
                    String error = jsonObject.getString("error");
                    if (error.equals("false")) {
                        JSONObject userData = new JSONObject();
                        userData = jsonObject.getJSONObject("data");
                        responseImage.add("true");
                        responseImage.add(userData.getString("profile_picture"));
                        callback.onLoaded(responseImage);
                    } else {
                        String message = jsonObject.getString("message");
                        responseImage.add("false");
                        responseImage.add(message);
                        callback.onLoaded(responseImage);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    responseImage.add("false");
                    responseImage.add(e.getMessage());
                    callback.onLoaded(responseImage);
                } catch (JSONException e) {
                    e.printStackTrace();
                    responseImage.add("false");
                    responseImage.add(e.getMessage());
                    callback.onLoaded(responseImage);
                }
            }
        }).start();
    }
}
