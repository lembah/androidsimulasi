package com.imm.androidsimulasi.Data;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.imm.androidsimulasi.Comment.CommentModel;
import com.imm.androidsimulasi.helper.AppConfig;
import com.imm.androidsimulasi.util.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lembah8 on 12/6/16.
 */

public class CommentServiceApiImpl implements CommentServiceApi {

    @Override
    public void viewComment(String videoId, final String token, final CommentServiceApi.CommentServiceCallback<CommentModel> callback, final CommentServiceApi.CommentServiceCallback<String> callbackMesage) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, AppConfig.URL_VIEW_COMMENT(videoId), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    boolean status = response.getBoolean("error");
                    Log.e("status", String.valueOf(status));
                    if (!status) {
                        System.out.println(response.toString());
//                        callbackMesage.onLoaded(message);
                        JSONArray jArrComment = response.getJSONArray("comments");
                        Log.e("JML", String.valueOf(jArrComment.length()));
                        if (jArrComment.length() == 0) {
                            callbackMesage.onLoaded("empty");
                            // Call Listener response if value equal null
                        } else {
                            // Call Listener response id value greater than null
                            List<CommentModel> commentModelList = new ArrayList<>();
                            try {
                                for (int i = 0; i < jArrComment.length(); i++) {
                                    JSONObject commentObj = jArrComment.getJSONObject(i);
                                    CommentModel commentModel = new CommentModel();
                                    commentModel.setUser_id(commentObj.getInt("user_id"));
                                    commentModel.setUser_name(commentObj.getString("name"));
                                    commentModel.setComment_id(commentObj.getString("comment_id"));
                                    //commentModel.setProfil_pic(commentObj.getString("profile_picture_url"));
                                    commentModel.setComment(commentObj.getString("comment"));
                                    commentModel.setCreate_at(commentObj.getString("created_at"));
                                    callback.onLoaded(commentModel);

                                }
                                callbackMesage.onLoaded("!empty");
                            } catch (JSONException e) {
                                callback.onLoaded(null);
                                callbackMesage.onLoaded("Sorry Something Error");
                            }
                            // Call Listener to notify adapter
                            // Call Listener to end getting view data
                        }

                    } else {
                        String message = response.getString("message");
                        callback.onLoaded(null);
                        callbackMesage.onLoaded(message);
                        // Call listener to response failed get comment data
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    callback.onLoaded(null);
                    callbackMesage.onLoaded(e.getMessage());
                    // Call listener to response failed get comment data
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error: " + error.getMessage());
                callbackMesage.onLoaded(error.getMessage());
                // Call listener to response failed get comment data
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", token);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    @Override
    public void writeComment(String videoId, String comment, final String token, final CommentServiceApi.CommentServiceCallback<String> callback) {
        String url = AppConfig.URL_SEND_COMMENT(videoId);
        HashMap<String, String> param = new HashMap<String, String>();
        param.put("comment", comment);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, new JSONObject(param),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println(response.toString());
                            String message = response.getString("message");
                            callback.onLoaded(message);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            callback.onLoaded(e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                callback.onLoaded(error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", token);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    @Override
    public void editComment(String commentId, String comment, final String token, final CommentServiceApi.CommentServiceCallback<String> callback) {
        String url = AppConfig.URL_EDIT_COMMENT + commentId;
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("comment", comment);

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.PUT, url, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println(response.toString());
                            String error = response.getString("error");
                            System.out.println(error);
                            if (error.equals("false")) {
                                // call Listener to response succesfull edit comment
                                callback.onLoaded("Comment edited");
                            } else {
                                String message = response.getString("message");
                                // call Listener to response failed edit comment
                                callback.onLoaded(message);
                            }
                            VolleyLog.v("Response:%n %s", response.toString(4));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            // Call Listener to response failed edit comment
                            callback.onLoaded(e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                VolleyLog.e("Error: ", error.getMessage());
                // Call Listener to response failed edit comment
                callback.onLoaded(error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", token);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req);
    }

    @Override
    public void deleteComment(String commentId, final String token, final CommentServiceApi.CommentServiceCallback<String> callback) {
        String url = AppConfig.URL_DELETE_COMMENT + commentId;
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.DELETE, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println(response.toString());
                            String error = response.getString("error");
                            System.out.println(error);
                            if (error.equals("false")) {
                                // Call Listener to response succesfull delete comment
                                callback.onLoaded("Comment deleted");
                            } else {
                                String message = response.getString("message");
                                // Call Listener to response failed delete comment
                                callback.onLoaded(message);
                            }
                            VolleyLog.v("Response:%n %s", response.toString(4));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            // Call Listener to response failed deleted comment
                            callback.onLoaded(e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                VolleyLog.e("Error: ", error.getMessage());
                // Call Listener to response failed delete comment
                callback.onLoaded(error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", token);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req);
    }
}
