package com.imm.androidsimulasi.Data;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.imm.androidsimulasi.helper.AppConfig;
import com.imm.androidsimulasi.util.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lembah8 on 12/6/16.
 */

public class VideoServiceApiImpl implements VideoServiceApi {
    @Override
    public void saveVideo() {

    }

    @Override
    public void deleteVideo(String videoId, final String token, final VideoServiceCallback callback) {
        String url = AppConfig.URL_DELETE_VIDEO + videoId;
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.DELETE, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println(response.toString());
                            String error = response.getString("error");
                            System.out.println(error);
                            if (error.equals("false")) {
                                System.out.println("ini berhasil");
                                callback.onLoaded("true");
                                // Call Listener to response succesfull delete comment
                            } else {
                                String message = response.getString("message");
                                System.out.println("ini"+message);
                                callback.onLoaded(message);
                                // Call Listener to response failed delete comment
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("ini"+e);
                            callback.onLoaded(e);
                            // Call Listener to response failed deleted comment
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                VolleyLog.e("Error: ", error.getMessage());
                System.out.println("ini"+error);
                callback.onLoaded(error);
                // Call Listener to response failed delete comment
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", token);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req);
    }

    @Override
    public void reportVideo(String videoId, String message) {

    }
}
