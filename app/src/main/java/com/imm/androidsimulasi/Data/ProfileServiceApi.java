package com.imm.androidsimulasi.Data;

import android.content.Context;
import android.net.Uri;

import com.imm.androidsimulasi.model.UpdateProfileModel;

import java.util.List;

/**
 * Created by lembah8 on 12/8/16.
 */

public interface ProfileServiceApi {
    interface ProfileServiceCallback<T> {
        void onLoaded(T message);
    }

    void UpdateProfile(String fullname, String userId, String token, String userAgent, ProfileServiceApi.ProfileServiceCallback<String> callbackMessage);

    void viewProfile(String userId, String token, String userAgent, ProfileServiceApi.ProfileServiceCallback<UpdateProfileModel> callback, ProfileServiceApi.ProfileServiceCallback<String> callbackMessage);

    void updateProfilPic(Uri imageUri, String token,  String userAgent, ProfileServiceApi.ProfileServiceCallback<List<String>> callback);
}
