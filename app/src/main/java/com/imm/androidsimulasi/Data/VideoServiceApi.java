package com.imm.androidsimulasi.Data;

/**
 * Created by lembah8 on 12/6/16.
 */

public interface VideoServiceApi {
    interface VideoServiceCallback<T> {

        void onLoaded(T message);
    }

    void saveVideo();
    void deleteVideo(String videoId, final String token, final VideoServiceCallback<String> callback);
    void reportVideo(String videoId, String message);
}
