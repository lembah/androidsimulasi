package com.imm.androidsimulasi.Data;

import com.imm.androidsimulasi.Comment.CommentModel;

/**
 * Created by lembah8 on 12/6/16.
 */

public interface CommentServiceApi {
    interface CommentServiceCallback<T> {

        void onLoaded(T message);
    }

    void viewComment(String videoId, String token, final CommentServiceApi.CommentServiceCallback<CommentModel> callback, CommentServiceApi.CommentServiceCallback<String> callbackMesage);
    void writeComment(String videoId, String comment, String token, final CommentServiceApi.CommentServiceCallback<String> callback);
    void editComment(String commentId, String comment, String token, final CommentServiceApi.CommentServiceCallback<String> callback);
    void deleteComment(String commentId, String token, final CommentServiceApi.CommentServiceCallback<String> callback);
}
