package com.imm.androidsimulasi.model;

/**
 * Created by lembah8 on 12/8/16.
 */

public class UpdateProfileModel {
    String userId;
    String userFullname;
    String userPic;

    public UpdateProfileModel(){

    }

    public void setUserId(String userId){
        this.userId = userId;
    }

    public String getUserId(){
        return userId;
    }

    public void setUserFullname(String userFullname){
        this.userFullname = userFullname;
    }

    public String getUserFullname(){
        return userFullname;
    }

    public void setUserPic(String imgUrl){
        this.userPic = imgUrl;
    }

    public String getUserPic(){
        return userPic;
    }
}
