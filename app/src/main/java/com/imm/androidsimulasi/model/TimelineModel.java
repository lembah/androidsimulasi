package com.imm.androidsimulasi.model;

/**
 * Created by lembah7 on 11/14/16.
 */

public class TimelineModel {
    String id;
    String user_id;
    String title;
    String description;
    String view;
    String like;
    String unlinke;
    String video_file_name;
    String video_content_type;
    String video_file_size;
    String created_at;
    String update_at;
    String topic_id;

    String video;
    String video_thumbnail;
    String uploader;


    public TimelineModel(){

    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getView() {
        return view;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public String getLike() {
        return like;
    }

    public void setUnlinke(String unlinke) {
        this.unlinke = unlinke;
    }

    public String getUnlinke() {
        return unlinke;
    }

    public void setVideo_file_name(String video_file_name) {
        this.video_file_name = video_file_name;
    }

    public String getVideo_file_name() {
        return video_file_name;
    }

    public void setVideo_content_type(String video_content_type) {
        this.video_content_type = video_content_type;
    }

    public String getVideo_file_size() {
        return video_file_size;
    }

    public void setVideo_file_size(String video_file_size) {
        this.video_file_size = video_file_size;
    }

    public String getVideo_content_type() {
        return video_content_type;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setTopic_id(String topic_id) {
        this.topic_id = topic_id;
    }

    public String getTopic_id() {
        return topic_id;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo_thumbnail(String video_thumbnail) {
        this.video_thumbnail = video_thumbnail;
    }

    public String getVideo_thumbnail() {
        return video_thumbnail;
    }

    public void setUploader(String uploader) {
        this.uploader = uploader;
    }

    public String getUploader() {
        return uploader;
    }
}
