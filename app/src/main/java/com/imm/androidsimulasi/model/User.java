package com.imm.androidsimulasi.model;

/**
 * Created by lembah8 on 11/14/16.
 */

public class User {
    String user_id;
    String user_name;
    String user_email;
    String token;

    public User(String user_id, String user_name, String user_email, String token)
    {
        this.user_id = user_id;
        this.user_name = user_name;
        this.user_email = user_email;
        this.token = token;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getUser_email(){
        return user_email;
    }

    public  String getToken(){
        return token;
    }
}
