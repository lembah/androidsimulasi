package com.imm.androidsimulasi.model;

/**
 * Created by yusufinthehouse on 6/18/16.
 */
public class IntentExtra {
    String key;
    String value;

    public IntentExtra(String key, String value) {
        this.value = value;
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
