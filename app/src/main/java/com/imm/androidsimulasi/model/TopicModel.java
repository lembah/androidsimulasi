package com.imm.androidsimulasi.model;

/**
 * Created by lembah8 on 11/23/16.
 */

public class TopicModel {
    int id;
    String topic;
    String thumbnail;

    public TopicModel(){}

    public void setId(int id){
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setTopic(String topic){
        this.topic = topic;
    }

    public String getTopic(){
        return topic;
    }

    public void setThumbnail(String thumbnail){
        this.thumbnail = thumbnail;
    }

    public String getThumbnail(){
        return thumbnail;
    }
}
