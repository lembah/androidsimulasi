package com.imm.androidsimulasi.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.imm.androidsimulasi.R;
import com.imm.androidsimulasi.ViewVideoDetail.ViewVideoDetailActivity;
import com.imm.androidsimulasi.activity.MainActivity;
import com.imm.androidsimulasi.adapter.TimelineAdapter;
import com.imm.androidsimulasi.helper.AppConfig;
import com.imm.androidsimulasi.helper.ProgressDialogCustom;
import com.imm.androidsimulasi.helper.SessionManager;
import com.imm.androidsimulasi.model.TimelineModel;
import com.imm.androidsimulasi.util.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class HomeFragment extends Fragment {
    private static String TAG = MainActivity.class.getSimpleName();

    private ListView listView;
    private TimelineAdapter adapter;
    private List<TimelineModel> timelineModelList;

    // Progress dialog
    private ProgressDialogCustom pDialog;

    String token;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        // Get user data from session
        final SessionManager session = new SessionManager(getActivity().getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        String name = user.get(SessionManager.KEY_NAME);
        token = user.get(SessionManager.KEY_TOKEN);

        listView            = (ListView) rootView.findViewById(R.id.listTimeline);
        timelineModelList   = new ArrayList<>();
        adapter             = new TimelineAdapter(getActivity(), timelineModelList, getActivity().getApplicationContext());
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Bundle bundle = new Bundle();
                bundle.putString("video", timelineModelList.get(i).getVideo());
                bundle.putString("video_id", timelineModelList.get(i).getId());
                bundle.putString("user_id", timelineModelList.get(i).getUser_id());
                bundle.putString("title", timelineModelList.get(i).getTitle());
                bundle.putString("desc", timelineModelList.get(i).getDescription());
                bundle.putString("topic", timelineModelList.get(i).getVideo_content_type());
                bundle.putString("like", timelineModelList.get(i).getLike());
                bundle.putString("unlike", timelineModelList.get(i).getUnlinke());
                bundle.putString("uploader", timelineModelList.get(i).getUploader());
                bundle.putString("create_date", timelineModelList.get(i).getCreated_at());

//                Intent intent = new Intent(getActivity(), VideoDetailActivity.class);
                Intent intent = new Intent(getActivity(), ViewVideoDetailActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        pDialog = new ProgressDialogCustom(this.getContext());

        getTimeline();

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void getTimeline(){
        pDialog.showpDialog();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                AppConfig.URL_TIMELINE, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                pDialog.hidepDialog();

                try {
                    boolean status = response.getBoolean("error");
                    //String status = response.getString("error");
                    Log.e("status", String.valueOf(status));

                    // Check error status
                    if (!status) {
                        JSONArray jArrTimeline = response.getJSONArray("timeline");
                        Log.e("JML", String.valueOf(jArrTimeline.length()));
                        // looping through json and adding to movies list
                        for (int i = 0; i < jArrTimeline.length(); i++) {
                            try {

                                JSONObject timelineObj = jArrTimeline.getJSONObject(i);
                                TimelineModel timelineModel = new TimelineModel();
                                timelineModel.setId(timelineObj.getString("id"));
                                timelineModel.setUser_id(timelineObj.getString("user_id"));
                                timelineModel.setTitle(timelineObj.getString("title"));
                                timelineModel.setDescription(timelineObj.getString("description"));
                                timelineModel.setView(timelineObj.getString("view"));
                                timelineModel.setLike(timelineObj.getString("like"));
                                timelineModel.setUnlinke(timelineObj.getString("unlike"));
                                timelineModel.setVideo(timelineObj.getString("video"));
                                timelineModel.setCreated_at(timelineObj.getString("created_at"));
                                timelineModel.setTopic_id(timelineObj.getString("topic_id"));
                                timelineModel.setVideo_thumbnail(timelineObj.getString("video_thumbnail"));
                                timelineModel.setUploader(timelineObj.getString("uploader"));
                                timelineModel.setVideo_content_type(timelineObj.getString("topic"));
                                //timelineModel.setVideo_file_name(timelineObj.getString("video_file_name"));

                                /*if (!timelineModelList.contains(timelineModel)){
                                    timelineModelList.add(timelineModel);
                                }*/
                                timelineModelList.add(timelineModel);

                            } catch (JSONException e) {
                                Log.e(TAG, "JSON Parsing error: " + e.getMessage());
                            }
                        }
                        adapter.notifyDataSetChanged();

                        listView.setClickable(true);

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = response.getString("message");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getActivity().getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
                pDialog.hidepDialog();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("token", token);

                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }


}
