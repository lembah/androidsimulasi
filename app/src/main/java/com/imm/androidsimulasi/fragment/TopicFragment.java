package com.imm.androidsimulasi.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.imm.androidsimulasi.R;
import com.imm.androidsimulasi.activity.MainActivity;
import com.imm.androidsimulasi.activity.VideoTopic;
import com.imm.androidsimulasi.adapter.TopicAdapter;
import com.imm.androidsimulasi.helper.AppConfig;
import com.imm.androidsimulasi.helper.ProgressDialogCustom;
import com.imm.androidsimulasi.helper.SessionManager;
import com.imm.androidsimulasi.model.TopicModel;
import com.imm.androidsimulasi.util.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TopicFragment extends Fragment {

    private static String TAG = MainActivity.class.getSimpleName();
    private GridView gridView;
    private TopicAdapter adapter;
    private List<TopicModel> topicModelList;

    // Progress dialog
    private ProgressDialogCustom pDialog;

    String token;

    public TopicFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_topic, container, false);

        // Get user data from session
        final SessionManager session = new SessionManager(getActivity().getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        token = user.get(SessionManager.KEY_TOKEN);

        gridView            = (GridView) rootView.findViewById(R.id.gridTopic);
        topicModelList   = new ArrayList<>();
        adapter             = new TopicAdapter(getActivity(), topicModelList, getActivity().getApplicationContext());
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                System.out.println("cliked");
                Bundle bundle = new Bundle();
                bundle.putInt("topic_id", topicModelList.get(i).getId());
                bundle.putString("topic", topicModelList.get(i).getTopic());
                Intent intent = new Intent(getActivity(), VideoTopic.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        pDialog = new ProgressDialogCustom(this.getContext());

        getTopicList();



        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void getTopicList(){
        pDialog.showpDialog();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                AppConfig.URL_TOPIC, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                pDialog.hidepDialog();

                try {
                    boolean status = response.getBoolean("error");
                    //String status = response.getString("error");
                    Log.e("status", String.valueOf(status));

                    // Check error status
                    if (!status) {
                        JSONArray jArrTopic = response.getJSONArray("data");
                        Log.e("JML", String.valueOf(jArrTopic.length()));
                        // looping through json and adding to movies list
                        for (int i = 0; i < jArrTopic.length(); i++) {
                            try {

                                JSONObject jsonObject = jArrTopic.getJSONObject(i);
                                TopicModel topicModel  = new TopicModel();
                                topicModel.setId(jsonObject.getInt("id"));
                                topicModel.setTopic(jsonObject.getString("title"));
                                topicModel.setThumbnail(jsonObject.getString("thumbnail"));

                                topicModelList.add(topicModel);

                            } catch (JSONException e) {
                                Log.e(TAG, "JSON Parsing error: " + e.getMessage());
                            }
                        }
                        adapter.notifyDataSetChanged();

                        gridView.setClickable(true);

                    } else {
                        // Error in login. Get the error message
                        String errorMsg = response.getString("message");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity().getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getActivity().getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
                pDialog.hidepDialog();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("token", token);

                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }
}
