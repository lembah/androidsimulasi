/*
 * MIT License
 *
 * Copyright (c) 2016 Knowledge, education for life.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.imm.androidsimulasi.util;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import com.imm.androidsimulasi.R;
import com.imm.androidsimulasi.util.OnRangeSeekBarListener;

public class CustomRangeSeekBarView extends View {

    private static final String TAG = CustomRangeSeekBarView.class.getSimpleName();

    private int mHeightTimeLine;
    private List<CustomThumb> mCustomThumbs;
    private List<OnRangeSeekBarListener> mListeners;
    private float mMaxWidth;
    private float mThumbWidth;
    private float mThumbHeight;
    private int mViewWidth;
    private float mPixelRangeMin;
    private float mPixelRangeMax;
    private float mScaleRangeMax;
    private boolean mFirstRun;

    private final Paint mShadow = new Paint();
    private final Paint mLine = new Paint();

    public CustomRangeSeekBarView(@NonNull Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomRangeSeekBarView(@NonNull Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mCustomThumbs = CustomThumb.initThumbs(getResources());
        mThumbWidth = CustomThumb.getWidthBitmap(mCustomThumbs);
        mThumbHeight = CustomThumb.getHeightBitmap(mCustomThumbs);

        mScaleRangeMax = 100;
        mHeightTimeLine = getContext().getResources().getDimensionPixelOffset(R.dimen.frames_video_height);

        setFocusable(true);
        setFocusableInTouchMode(true);

        mFirstRun = true;

        int shadowColor = ContextCompat.getColor(getContext(), R.color.black_translucent);
        mShadow.setAntiAlias(true);
        mShadow.setColor(shadowColor);
        mShadow.setAlpha(177);

        int lineColor = ContextCompat.getColor(getContext(), R.color.colorPrimary);
        mLine.setAntiAlias(true);
        mLine.setColor(lineColor);
        mLine.setAlpha(200);
    }

    public void initMaxWidth() {
        mMaxWidth = mCustomThumbs.get(1).getPos() - mCustomThumbs.get(0).getPos();
        System.out.println(String.format("Max width : %s", mMaxWidth));

        onSeekStop(this, 0, mCustomThumbs.get(0).getVal());
        onSeekStop(this, 1, mCustomThumbs.get(1).getVal());

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int minW = getPaddingLeft() + getPaddingRight() + getSuggestedMinimumWidth();
        mViewWidth = resolveSizeAndState(minW, widthMeasureSpec, 1);

        int minH = getPaddingBottom() + getPaddingTop() + (int) mThumbHeight + mHeightTimeLine;
        int viewHeight = resolveSizeAndState(minH, heightMeasureSpec, 1);

        setMeasuredDimension(mViewWidth, viewHeight);

        mPixelRangeMin = 0;
        mPixelRangeMax = mViewWidth - mThumbWidth;

        if (mFirstRun) {
            for (int i = 0; i < mCustomThumbs.size(); i++) {
                CustomThumb th = mCustomThumbs.get(i);
                th.setVal(mScaleRangeMax * i);
                th.setPos(mPixelRangeMax * i);
            }
            // Fire listener callback
            onCreate(this, currentThumb, getThumbValue(currentThumb));
            mFirstRun = false;
        }
    }

    @Override
    protected void onDraw(@NonNull Canvas canvas) {
        super.onDraw(canvas);

        drawShadow(canvas);
        drawThumbs(canvas);
    }

    private int currentThumb = 0;

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent ev) {
        final CustomThumb mCustomThumb;
        final CustomThumb mCustomThumb2;
        final float coordinate = ev.getX();
        final int action = ev.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN: {
                // Remember where we started
                currentThumb = getClosestThumb(coordinate);

                if (currentThumb == -1) {
                    return false;
                }

                mCustomThumb = mCustomThumbs.get(currentThumb);
                mCustomThumb.setLastTouchX(coordinate);
                onSeekStart(this, currentThumb, mCustomThumb.getVal());
                return true;
            }
            case MotionEvent.ACTION_UP: {

                if (currentThumb == -1) {
                    return false;
                }

                mCustomThumb = mCustomThumbs.get(currentThumb);
                onSeekStop(this, currentThumb, mCustomThumb.getVal());
                return true;
            }

            case MotionEvent.ACTION_MOVE: {
                mCustomThumb = mCustomThumbs.get(currentThumb);
                mCustomThumb2 = mCustomThumbs.get(currentThumb == 0 ? 1 : 0);
                // Calculate the distance moved
                final float dx = coordinate - mCustomThumb.getLastTouchX();
                final float newX = mCustomThumb.getPos() + dx;
                if (currentThumb == 0) {

                    if ((newX + mCustomThumb.getWidthBitmap()) >= mCustomThumb2.getPos()) {
                        mCustomThumb.setPos(mCustomThumb2.getPos() - mCustomThumb.getWidthBitmap());
                    } else if (newX <= mPixelRangeMin) {
                        mCustomThumb.setPos(mPixelRangeMin);
                    } else {
                        //Check if thumb is not out of max width
                        checkPositionThumb(mCustomThumb, mCustomThumb2, dx, true);
                        // Move the object
                        mCustomThumb.setPos(mCustomThumb.getPos() + dx);

                        // Remember this touch position for the next move event
                        mCustomThumb.setLastTouchX(coordinate);
                    }

                } else {
                    if (newX <= mCustomThumb2.getPos() + mCustomThumb2.getWidthBitmap()) {
                        mCustomThumb.setPos(mCustomThumb2.getPos() + mCustomThumb.getWidthBitmap());
                    } else if (newX >= mPixelRangeMax) {
                        mCustomThumb.setPos(mPixelRangeMax);
                    } else {
                        //Check if thumb is not out of max width
                        checkPositionThumb(mCustomThumb2, mCustomThumb, dx, false);
                        // Move the object
                        mCustomThumb.setPos(mCustomThumb.getPos() + dx);
                        // Remember this touch position for the next move event
                        mCustomThumb.setLastTouchX(coordinate);
                    }
                }

                setThumbPos(currentThumb, mCustomThumb.getPos());

                // Invalidate to request a redraw
                invalidate();
                return true;
            }
        }
        return false;
    }

    private void checkPositionThumb(@NonNull CustomThumb mCustomThumbLeft, @NonNull CustomThumb mCustomThumbRight, float dx, boolean isLeftMove) {
        if (isLeftMove && dx < 0) {
            if ((mCustomThumbRight.getPos() - (mCustomThumbLeft.getPos() + dx)) > mMaxWidth) {
                mCustomThumbRight.setPos(mCustomThumbLeft.getPos() + dx + mMaxWidth);
                setThumbPos(1, mCustomThumbRight.getPos());
            }
        } else if (!isLeftMove && dx > 0) {
            if (((mCustomThumbRight.getPos() + dx) - mCustomThumbLeft.getPos()) > mMaxWidth) {
                mCustomThumbLeft.setPos(mCustomThumbRight.getPos() + dx - mMaxWidth);
                setThumbPos(0, mCustomThumbLeft.getPos());
            }
        }
    }

    private int getUnstuckFrom(int index) {
        int unstuck = 0;
        float lastVal = mCustomThumbs.get(index).getVal();
        for (int i = index - 1; i >= 0; i--) {
            CustomThumb th = mCustomThumbs.get(i);
            if (th.getVal() != lastVal)
                return i + 1;
        }
        return unstuck;
    }

    private float pixelToScale(int index, float pixelValue) {
        float scale = (pixelValue * 100) / mPixelRangeMax;
        if (index == 0) {
            float pxThumb = (scale * mThumbWidth) / 100;
            return scale + (pxThumb * 100) / mPixelRangeMax;
        } else {
            float pxThumb = ((100 - scale) * mThumbWidth) / 100;
            return scale - (pxThumb * 100) / mPixelRangeMax;
        }
    }

    private float scaleToPixel(int index, float scaleValue) {
        float px = (scaleValue * mPixelRangeMax) / 100;
        if (index == 0) {
            float pxThumb = (scaleValue * mThumbWidth) / 100;
            return px - pxThumb;
        } else {
            float pxThumb = ((100 - scaleValue) * mThumbWidth) / 100;
            return px + pxThumb;
        }
    }

    private void calculateThumbValue(int index) {
        if (index < mCustomThumbs.size() && !mCustomThumbs.isEmpty()) {
            CustomThumb th = mCustomThumbs.get(index);
            th.setVal(pixelToScale(index, th.getPos()));
            onSeek(this, index, th.getVal());
        }
    }

    private void calculateThumbPos(int index) {
        if (index < mCustomThumbs.size() && !mCustomThumbs.isEmpty()) {
            CustomThumb th = mCustomThumbs.get(index);
            th.setPos(scaleToPixel(index, th.getVal()));
        }
    }

    private float getThumbValue(int index) {
        return mCustomThumbs.get(index).getVal();
    }

    public void setThumbValue(int index, float value) {
        mCustomThumbs.get(index).setVal(value);
        calculateThumbPos(index);
        // Tell the view we want a complete redraw
        invalidate();
    }

    private void setThumbPos(int index, float pos) {
        mCustomThumbs.get(index).setPos(pos);
        calculateThumbValue(index);
        // Tell the view we want a complete redraw
        invalidate();
    }

    private int getClosestThumb(float coordinate) {
        int closest = -1;
        if (!mCustomThumbs.isEmpty()) {
            for (int i = 0; i < mCustomThumbs.size(); i++) {
                // Find thumb closest to x coordinate
                final float tcoordinate = mCustomThumbs.get(i).getPos() + mThumbWidth;
                if (coordinate >= mCustomThumbs.get(i).getPos() && coordinate <= tcoordinate) {
                    closest = mCustomThumbs.get(i).getIndex();
                }
            }
        }
        return closest;
    }

    private void drawShadow(@NonNull Canvas canvas) {
        if (!mCustomThumbs.isEmpty()) {

            for (CustomThumb th : mCustomThumbs) {
                if (th.getIndex() == 0) {
                    final float x = th.getPos() + getPaddingLeft();
                    if (x > mPixelRangeMin) {
                        Rect mRect = new Rect((int) mThumbWidth, 0, (int) (x + mThumbWidth), mHeightTimeLine);
                        canvas.drawRect(mRect, mShadow);
                    }
                } else {
                    final float x = th.getPos() - getPaddingRight();
                    if (x < mPixelRangeMax) {
                        Rect mRect = new Rect((int) x, 0, (int) (mViewWidth - mThumbWidth), mHeightTimeLine);
                        canvas.drawRect(mRect, mShadow);
                    }
                }
            }
        }
    }

    private void drawThumbs(@NonNull Canvas canvas) {

        if (!mCustomThumbs.isEmpty()) {
            for (CustomThumb th : mCustomThumbs) {
                if (th.getIndex() == 0) {
//                    canvas.drawBitmap(th.getBitmap(), th.getPos() + getPaddingLeft(), getPaddingTop(), null);
                    canvas.drawBitmap(th.getBitmap(), th.getPos() + getPaddingLeft(), getPaddingTop() + mHeightTimeLine, null);
                } else {
//                    canvas.drawBitmap(th.getBitmap(), th.getPos() - getPaddingRight(), getPaddingTop(), null);
                    canvas.drawBitmap(th.getBitmap(), th.getPos() - getPaddingRight(), getPaddingTop() + mHeightTimeLine, null);
                }
            }
        }
    }

    public void addOnRangeSeekBarListener(OnRangeSeekBarListener listener) {

        if (mListeners == null) {
            mListeners = new ArrayList<>();
        }

        mListeners.add(listener);
    }

    private void onCreate(CustomRangeSeekBarView customRangeSeekBarView, int index, float value) {
        if (mListeners == null)
            return;

        for (OnRangeSeekBarListener item : mListeners) {
            item.onCreate(customRangeSeekBarView, index, value);
        }
    }

    private void onSeek(CustomRangeSeekBarView customRangeSeekBarView, int index, float value) {
        if (mListeners == null)
            return;

        for (OnRangeSeekBarListener item : mListeners) {
            item.onSeek(customRangeSeekBarView, index, value);
        }
    }

    private void onSeekStart(CustomRangeSeekBarView customRangeSeekBarView, int index, float value) {
        if (mListeners == null)
            return;

        for (OnRangeSeekBarListener item : mListeners) {
            item.onSeekStart(customRangeSeekBarView, index, value);
        }
    }

    private void onSeekStop(CustomRangeSeekBarView customRangeSeekBarView, int index, float value) {
        if (mListeners == null)
            return;

        for (OnRangeSeekBarListener item : mListeners) {
            item.onSeekStop(customRangeSeekBarView, index, value);
        }
    }

    public List<CustomThumb> getThumbs() {
        return mCustomThumbs;
    }
}
