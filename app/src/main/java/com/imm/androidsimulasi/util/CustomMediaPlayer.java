package com.imm.androidsimulasi.util;

import android.media.MediaPlayer;

import java.io.IOException;

/**
 * Created by lembah8 on 12/22/16.
 */

public class CustomMediaPlayer extends MediaPlayer {

    String dataSource = null;

    @Override
    public void setDataSource(String path) throws IOException {
        super.setDataSource(path);
        dataSource = path;
    }

    public String getDataSource()
    {
        return dataSource;
    }
}
