package com.imm.androidsimulasi.VideoDetail;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.imm.androidsimulasi.R;


public class VideoDetailFragment extends Fragment {

    TextView txtTitle;
    TextView txtTopic;
    TextView txtDesc;
    TextView txtUserName;
    TextView txtCreateDate;
    ImageView imgVideoOption;

    String title;
    String topic;
    String desc;
    String userName;
    String createDate;


    public VideoDetailFragment(String title, String topic, String desc, String userName, String createDate) {
        this.title = title;
        this.topic = topic;
        this.desc = desc;
        this.userName = userName;
        this.createDate = createDate;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_video_detail, container, false);

        txtTitle = (TextView) rootView.findViewById(R.id.txt_detail_title);
        txtTopic = (TextView) rootView.findViewById(R.id.txt_detail_topic);
        txtDesc = (TextView) rootView.findViewById(R.id.txt_detail_desc);
        txtUserName = (TextView) rootView.findViewById(R.id.txt_detail_uploader);
        txtCreateDate = (TextView) rootView.findViewById(R.id.txt_detail_create_date);
        imgVideoOption = (ImageView) rootView.findViewById(R.id.img_detail_option_video);

        txtTitle.setText(title);
        txtTopic.setText(topic);
        txtDesc.setText(desc);
        txtUserName.setText(userName);
        txtCreateDate.setText(createDate);


        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
    }
}
