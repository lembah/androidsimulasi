package com.imm.androidsimulasi.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import com.imm.androidsimulasi.VideoEdit.VideoEditActivity;
import com.imm.androidsimulasi.R;
import com.imm.androidsimulasi.UpdateProfile.UpdateProfileActivity;
import com.imm.androidsimulasi.fragment.HomeFragment;
import com.imm.androidsimulasi.fragment.NotificationFragment;
import com.imm.androidsimulasi.fragment.TopicFragment;
import com.imm.androidsimulasi.helper.SessionManager;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    SessionManager session;
    FloatingActionButton fab;
    FloatingActionButton fabVideo;
    FloatingActionButton fabGalery;

    Boolean fabStatus = false;

    /* ditambah untuk request video */
    static final int REQUEST_VIDEO_CAPTURE = 1;
    private static final int SELECT_VIDEO = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Session manager
        session = new SessionManager(getApplicationContext());

        // Check if user is already logged in or not

        /*if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            IntentHelper.redirectActivity(MainActivity.this, LoginActivity.class);
            finish();
        }*/


        fab = (FloatingActionButton) findViewById(R.id.fab);
        fabVideo = (FloatingActionButton) findViewById(R.id.fab_camera);
        fabGalery = (FloatingActionButton) findViewById(R.id.fab_galery);

        fabGalery.setVisibility(ViewGroup.GONE);
        fabVideo.setVisibility(ViewGroup.GONE);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(fabStatus == false){
                    fab.setImageResource(R.drawable.ic_clear);
                    fabVideo.setVisibility(ViewGroup.VISIBLE);
                    fabGalery.setVisibility(ViewGroup.VISIBLE);
                }else{
                    fab.setImageResource(R.drawable.ic_add);
                    fabVideo.setVisibility(ViewGroup.GONE);
                    fabGalery.setVisibility(ViewGroup.GONE);
                }
                fabStatus = !fabStatus;

//                dispatchTakeVideoIntent();
                /*
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                        */
            }
        });

        fabVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dispatchTakeVideoIntent();
//                Intent intent = new Intent();
//                intent.setType("video/*");
//                intent.setAction(Intent.ACTION_GET_CONTENT);
//                startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_VIDEO_CAPTURE);
            }
        });

        fabGalery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("video/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Video"), SELECT_VIDEO);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Fragment fragment   = new HomeFragment();
        String title        = getString(R.string.title_home);
        fragmentManagement(fragment, title);
    }

    /* start rekam video */
    private void dispatchTakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
            Intent i = new Intent(MainActivity.this, VideoEditActivity.class);
            Uri videoUri = intent.getData();
            String videoUriString = videoUri.toString();
            i.putExtra("videoUri",videoUriString);
            startActivity(i);
        }
        if (requestCode == SELECT_VIDEO && resultCode == RESULT_OK) {
            Intent i = new Intent(MainActivity.this, VideoEditActivity.class);
            Uri videoUri = intent.getData();
            String videoUriString = videoUri.toString();
            i.putExtra("videoUri",videoUriString);
            startActivity(i);
        }
        if(resultCode == RESULT_CANCELED){

        }
    }
    /* stop rekam video */

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Fragment fragment = null;
        String title = getString(R.string.app_name);

        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
            fragment = new HomeFragment();
            fab.setVisibility(View.VISIBLE);
            title = getString(R.string.title_home);
        } else if (id == R.id.nav_gallery) {
            fragment = new TopicFragment();
            fab.setVisibility(View.GONE);
            title = getString(R.string.title_topic);
        } else if (id == R.id.nav_slideshow) {
            /*fragment = new ProfileFragment();
            fab.setVisibility(View.GONE);
            title = getString(R.string.title_profile);*/
            Intent i = new Intent(MainActivity.this, UpdateProfileActivity.class);
            startActivity(i);
//            fragment = new UpdateProfileFragment();
//            fab.setVisibility(View.GONE);
            title = getString(R.string.title_profile);
        } else if (id == R.id.nav_manage) {
            fragment = new NotificationFragment();
            fab.setVisibility(View.GONE);
            title = getString(R.string.title_notification);
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_logout) {
            SessionManager session = new SessionManager(getApplicationContext());
            session.logoutUser();
        }

        fragmentManagement(fragment, title);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;


    }

    private void fragmentManagement(Fragment fragment, String title){
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();

            // set the toolbar title
            getSupportActionBar().setTitle(title);
        }
    }

}
