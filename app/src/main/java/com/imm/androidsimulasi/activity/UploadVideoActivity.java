package com.imm.androidsimulasi.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.imm.androidsimulasi.R;
import com.imm.androidsimulasi.helper.AppConfig;
import com.imm.androidsimulasi.helper.IntentHelper;
import com.imm.androidsimulasi.helper.MultipartHelper;
import com.imm.androidsimulasi.helper.SessionManager;
import com.imm.androidsimulasi.util.AppController;
import com.imm.androidsimulasi.util.SpaceTokenizer;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class UploadVideoActivity extends AppCompatActivity {

    VideoView videoView;
    private int position = 0;
    private MediaController mediaController;
    private EditText gTitle;
    private AutoCompleteTextView gTopic;
    private MultiAutoCompleteTextView gDesc;
    private Button gUpload;
    private ProgressDialog gDialog;
    Hashtable<String, String> arrHastag;
    ArrayAdapter adapter;
    SessionManager session;
    Hashtable<String, String> arrTopics;
    private String token;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_video);
        initToolBar();
        // Session manager
        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        token = user.get(SessionManager.KEY_TOKEN);

        /* preview video */
        String videoUriString = getIntent().getStringExtra("videoUri");
        Log.d("URI 2", videoUriString);
        final Uri videoUri = Uri.parse(videoUriString);
        videoView = (VideoView) findViewById(R.id.vdo_video);
        videoView.setVideoURI(videoUri);

        if (mediaController == null) {
            mediaController = new MediaController(UploadVideoActivity.this);
            mediaController.setAnchorView(videoView);
            videoView.setMediaController(mediaController);
        }

        try {
            videoView.setVideoURI(videoUri);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }

        videoView.requestFocus();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                videoView.seekTo(position);
                if (position == 0) {
                    Log.d("video", "playing");
                    videoView.start();
                }
                mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i1) {
                        mediaController.setAnchorView(videoView);
                    }
                });
            }
        });

        gTitle = (EditText) findViewById(R.id.edt_title);
        gTopic = (AutoCompleteTextView) findViewById(R.id.edt_topic);
        gDesc = (MultiAutoCompleteTextView) findViewById(R.id.edt_desc);
        gDesc.setTokenizer(new SpaceTokenizer());
        // melakukan setup untuk seuggestion topic
        arrTopics = new Hashtable<String, String>();
        arrHastag = new Hashtable<String, String>();
        setupTopic();
        setupHastag();

        gUpload = (Button) findViewById(R.id.btn_upload);
        gUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptUpload(getPath(videoUri));
            }
        });
    }

    private void setupHastag() {
//        arrHastag.put("#mantap", "1");
//        arrHastag.put("#josss", "2");
//        arrHastag.put("#indonesia", "3");
//        arrHastag.put("#jokowi", "4");
//        arrHastag.put("#jaketJokowi", "5");
        ArrayList<String> dataListHastag = new ArrayList<String>();
        dataListHastag.add("#mantap");
        dataListHastag.add("#joss");
        dataListHastag.add("#indonesia");
        dataListHastag.add("#jokowi");
        ArrayAdapter<String> adapter = new ArrayAdapter(UploadVideoActivity.this, android.R.layout.simple_spinner_dropdown_item, dataListHastag);
        gDesc.setThreshold(1);
        gDesc.setAdapter(adapter);
    }

    /**
     * fungsi untuk melakukan setup untuk suggestion pada topic
     **/
    private void setupTopic() {
        gDialog = ProgressDialog.show(UploadVideoActivity.this, "", "Loading...", true);
        String urlTopic = AppConfig.URL_TOPIC;
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, urlTopic, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String error = response.getString("error");
                            if (error.equals("false")) {
                                JSONArray jsonArray = response.getJSONArray("data");
                                {
                                    ArrayList<String> datalistTopic = new ArrayList<String>();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        String id = jsonObject.getString("id");
                                        String topic = jsonObject.getString("title");
                                        arrTopics.put(topic, id);
                                        datalistTopic.add(jsonObject.getString("title"));
                                    }
                                    adapter = new ArrayAdapter(UploadVideoActivity.this, android.R.layout.simple_spinner_dropdown_item, datalistTopic);
                                    gTopic.setAdapter(adapter);
                                    gTopic.setThreshold(1);
                                    gDialog.dismiss();
                                }
                            } else {
                                gDialog.dismiss();
                                String message = response.getString("message");
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            gDialog.dismiss();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        gDialog.dismiss();
                        error.printStackTrace();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", token);

                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }


    /**
     * fungsi bawaan untuk memutar video
     **/
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt("CurrentPosition", videoView.getCurrentPosition());
        videoView.pause();
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstaceState) {
        super.onRestoreInstanceState(savedInstaceState);

        position = savedInstaceState.getInt("CurrentPosition");
        videoView.seekTo(position);
    }
    /** end fungsi**/


    /**
     * fungsi yang digunakan untuk mendapatkan path dari video yang direkam
     **/
    private String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA, MediaStore.Video.Media.SIZE, MediaStore.Video.Media.DURATION};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        cursor.moveToFirst();
        String filePath = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA));

        return filePath;
    }

    /**
     * fungsi yang digunakan untuk melakukan pengecekan input
     **/
    private void attemptUpload(final String pathVideo) {
        gTitle.setError(null);
        gTopic.setError(null);
        gDesc.setError(null);

        final String lJudul = gTitle.getText().toString();
        final String lTopik = gTopic.getText().toString();
        final String lDesc = gDesc.getText().toString();
        final String lPath = pathVideo;

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(lJudul)) {
            gTitle.setError(getString(R.string.error_field_required));
            focusView = gTitle;
            cancel = true;
        }

        if (TextUtils.isEmpty(lTopik)) {
            gTopic.setError(getString(R.string.error_field_required));
            focusView = gTopic;
            cancel = true;
        } else if (!arrTopics.containsKey(lTopik)) {
            gTopic.setError(getString(R.string.error_topic_undefined));
            focusView = gTopic;
            cancel = true;
        }

        if (TextUtils.isEmpty(lDesc)) {
            gDesc.setError(getString(R.string.error_field_required));
            focusView = gDesc;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            // Memulai melakukan upload data
            gDialog = ProgressDialog.show(UploadVideoActivity.this, "", "Uploading video...", true);
            new Thread(new Runnable() {
                @Override
                public void run() {

                    String requestURL = AppConfig.URL_UPLOAD;
                    try {
                        MultipartHelper multipart = new MultipartHelper(requestURL);
                        multipart.addRequestMehod("POST");
                        multipart.addHeaderField("token", token);
                        multipart.start();
                        multipart.addFormField("topic_id", arrTopics.get(lTopik));
                        multipart.addFormField("title", lJudul);
                        multipart.addFormField("description", lDesc);
                        multipart.addFilePart("video", lPath);
                        String response = multipart.finish();
                        Log.d("SERVER REPLIED", response);

                        JSONObject jsonObject = new JSONObject(response);
                        String error = jsonObject.getString("error");
                        if (error.equals("false")) {
                            gDialog.dismiss();
                            ToastMessage("Upload Succesfull");
                            IntentHelper.redirectActivity(UploadVideoActivity.this, MainActivity.class);

                        } else {
                            String message = jsonObject.getString("message");
                            if (message.equals("Topic must exist")) {
                                TopicInputError();
                            }
                            gDialog.dismiss();
                            ToastMessage(message);
                        }
                        gDialog.dismiss();

                    } catch (IOException e) {
                        gDialog.dismiss();
                        e.printStackTrace();
                        ToastMessage("Sorry something error");
                    } catch (JSONException e) {
                        gDialog.dismiss();
                        e.printStackTrace();
                        ToastMessage("Sorry something error");
                    }
                }
            }).start();
        }
    }

    /**
     * fungsi yang digunakan untuk memunculkan Toast didalam Thread
     **/
    void ToastMessage(String Message) {
        final String message = Message;
        Activity mActivity = UploadVideoActivity.this;
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * fungsi yang digunakan untuk memunculkan Error Focuc pada input di dalam Thread
     **/
    void TopicInputError() {
        Activity mActivity = UploadVideoActivity.this;
        mActivity.runOnUiThread(new Runnable() {
            public void run() {
                gTopic.setError(getString(R.string.error_topic_undefined));
                View focusView = gTopic;
                focusView.requestFocus();
            }
        });
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar_upload);
        toolbar.setTitle("Upload Video");

        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentHelper.backActivity(UploadVideoActivity.this, MainActivity.class);
            }
        });
    }

}
