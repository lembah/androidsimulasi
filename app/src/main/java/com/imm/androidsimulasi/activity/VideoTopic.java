package com.imm.androidsimulasi.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.imm.androidsimulasi.R;
import com.imm.androidsimulasi.ViewVideoDetail.ViewVideoDetailActivity;
import com.imm.androidsimulasi.adapter.VideoTopicAdapter;
import com.imm.androidsimulasi.helper.AppConfig;
import com.imm.androidsimulasi.helper.SessionManager;
import com.imm.androidsimulasi.model.TimelineModel;
import com.imm.androidsimulasi.util.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class VideoTopic extends AppCompatActivity {

    private ListView listView;
    private VideoTopicAdapter adapter;
    private List<TimelineModel> timelineModelList;
    private SessionManager session;
    private LinearLayout layout;
    private RelativeLayout progLayout;
    // Progress dialog
//    private ProgressDialogCustom pDialog;

    String token;
    int topicId;
    String topicTitle;
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_topic);

        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        String name = user.get(SessionManager.KEY_NAME);
        token = user.get(SessionManager.KEY_TOKEN);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            topicId = extras.getInt("topic_id");
            topicTitle = extras.getString("topic");
        }
        initToolBar();


        Log.d("topic id", String.valueOf(topicId));
//        pDialog = new ProgressDialogCustom(getApplicationContext());
        layout = (LinearLayout) findViewById(R.id.topic_not_found);
        layout.setVisibility(View.GONE);

        progLayout = (RelativeLayout) findViewById(R.id.prog_videoTopic);

        listView = (ListView) findViewById(R.id.listVideoTopic);
        timelineModelList = new ArrayList<>();
        adapter = new VideoTopicAdapter(timelineModelList, VideoTopic.this);
        listView.setAdapter(adapter);
        getVideoList(String.valueOf(topicId));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Bundle bundle = new Bundle();
                bundle.putString("video", timelineModelList.get(i).getVideo());
                bundle.putString("video_id", timelineModelList.get(i).getId());
                bundle.putString("user_id", timelineModelList.get(i).getUser_id());
                bundle.putString("title", timelineModelList.get(i).getTitle());
                bundle.putString("desc", timelineModelList.get(i).getDescription());
                bundle.putString("topic", timelineModelList.get(i).getVideo_content_type());
                bundle.putString("like", timelineModelList.get(i).getLike());
                bundle.putString("unlike", timelineModelList.get(i).getUnlinke());
                bundle.putString("uploader", timelineModelList.get(i).getUploader());
                bundle.putString("create_date", timelineModelList.get(i).getCreated_at());
                bundle.putString("topic", timelineModelList.get(i).getVideo_content_type());


                Intent intent = new Intent(VideoTopic.this, ViewVideoDetailActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void getVideoList(String videoId){

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                AppConfig.URL_VIDEO_TOPIC(videoId), null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
//                pDialog.hidepDialog();

                try {
                    System.out.println(response);
                    boolean status = response.getBoolean("error");
                    //String status = response.getString("error");
                    Log.e("status", String.valueOf(status));

                    // Check error status
                    if (!status) {
                        JSONArray jArrTimeline = response.getJSONArray("timeline");
                        Log.e("JML", String.valueOf(jArrTimeline.length()));
                        int jml = jArrTimeline.length();
                        if(jml < 1)
                        {
                            layout.setVisibility(View.VISIBLE);
                        }
                        // looping through json and adding to movies list
                        for (int i = 0; i < jArrTimeline.length(); i++) {
                            try {

                                JSONObject timelineObj = jArrTimeline.getJSONObject(i);
                                TimelineModel timelineModel = new TimelineModel();
                                timelineModel.setId(timelineObj.getString("id"));
                                timelineModel.setTitle(timelineObj.getString("title"));
                                timelineModel.setDescription(timelineObj.getString("description"));
                                timelineModel.setVideo(timelineObj.getString("video"));
                                timelineModel.setVideo_thumbnail(timelineObj.getString("video_thumbnail"));
                                timelineModel.setUser_id(timelineObj.getString("user_id"));
                                timelineModel.setUploader(timelineObj.getString("uploader"));
                                //email
                                timelineModel.setView(timelineObj.getString("view"));
                                timelineModel.setLike(timelineObj.getString("like"));
                                timelineModel.setUnlinke(timelineObj.getString("unlike"));
                                timelineModel.setCreated_at(timelineObj.getString("created_at"));
                                timelineModel.setVideo_content_type(response.getString("title"));
                                //timelineModel.setVideo_file_name(timelineObj.getString("video_file_name"));

                                /*if (!timelineModelList.contains(timelineModel)){
                                    timelineModelList.add(timelineModel);
                                }*/
                                timelineModelList.add(timelineModel);

                            } catch (JSONException e) {
                                Log.e("JSON Parsing error: ", e.getMessage());
                            }
                        }
                        adapter.notifyDataSetChanged();
                        progLayout.setVisibility(View.GONE);
                        listView.setClickable(true);

                    } else {
                        // Error in login. Get the error message
                        progLayout.setVisibility(View.GONE);
                        String errorMsg = response.getString("message");
                        System.out.println(errorMsg);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    progLayout.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error: " + error.getMessage());
                progLayout.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();
                // hide the progress dialog
//                pDialog.hidepDialog();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("token", token);

                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    public void initToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar_videoTopic);
        toolbar.setTitle(topicTitle);

        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              onBackPressed();
            }
        });
    }
}
