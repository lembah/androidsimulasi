package com.imm.androidsimulasi.VideoView;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.imm.androidsimulasi.R;


public class VideoViewFragment extends Fragment implements VideoViewContract.view {

    private VideoView videoView;
    private ProgressBar videoProgress;
    private MediaController mediacontroller;
    private Context context;
    private String videoUrl;

    private VideoViewContract.actionListener videoListener;


    public VideoViewFragment(Context context, String videoUrl, String videoUserId, String userId) {
        this.context = context;
        this.videoUrl = videoUrl;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        videoListener = new VideoViewPresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_video_view, container, false);

        videoView =  (VideoView) rootView.findViewById(R.id.video_view);
        videoProgress = (ProgressBar) rootView.findViewById(R.id.prog_video_view);

        videoListener.setupVideo();

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface OnFragmentInteractionListener {
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void prepareVideo(){
        new prepareVideoAsy().execute();
    }

    public class prepareVideoAsy extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mediacontroller = new MediaController(context);
        }

        @Override
        protected String doInBackground(String... params) {
            Uri video = Uri.parse(videoUrl);
            videoView.setMediaController(mediacontroller);
            videoView.setVideoURI(video);
            videoView.requestFocus();
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                // Close the progress bar and play the video
                public void onPrepared(MediaPlayer mp) {
                    videoView.start();
                    videoProgress.setVisibility(View.GONE);
                }
            });
            return "All Done!";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            mediacontroller.setAnchorView(videoView);
        }
    }
}
