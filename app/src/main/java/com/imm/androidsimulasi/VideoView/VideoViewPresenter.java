package com.imm.androidsimulasi.VideoView;

/**
 * Created by lembah8 on 12/6/16.
 */

public class VideoViewPresenter implements VideoViewContract.actionListener{

    VideoViewContract.view videoViewView;

    public VideoViewPresenter(VideoViewContract.view videoViewView){
        this.videoViewView = videoViewView;
    }

    @Override
    public void setupVideo() {
        videoViewView.prepareVideo();
    }
}
