package com.imm.androidsimulasi.VideoView;

/**
 * Created by lembah8 on 12/6/16.
 */

public interface VideoViewContract {
    interface view{
        void prepareVideo();
    }

    interface  actionListener{
        void setupVideo();
    }
}
