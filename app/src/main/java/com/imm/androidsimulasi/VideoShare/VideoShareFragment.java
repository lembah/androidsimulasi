package com.imm.androidsimulasi.VideoShare;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.imm.androidsimulasi.R;


public class VideoShareFragment extends Fragment {

    ImageView imgShare;

    public VideoShareFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_video_share, container, false);
        imgShare = (ImageView) rootView.findViewById(R.id.img_detail_share);

        imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingInteng = new Intent(Intent.ACTION_SEND);
                sharingInteng.putExtra(Intent.EXTRA_TEXT, "https://www.youtube.com/watch?v=G4tXtkp9SNo");
                sharingInteng.setType("text/plain");
                startActivity(Intent.createChooser(sharingInteng, "Share Via"));
            }
        });

        return rootView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface OnFragmentInteractionListener {
    }
}
