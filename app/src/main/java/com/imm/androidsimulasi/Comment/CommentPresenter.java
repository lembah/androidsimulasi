package com.imm.androidsimulasi.Comment;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.imm.androidsimulasi.Data.CommentServiceApi;
import com.imm.androidsimulasi.Data.CommentServiceApiImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lembah8 on 12/6/16.
 */

public class CommentPresenter implements CommentContract.actionListener {

    CommentContract.view commentViewView;
    CommentServiceApi commentServiceApi;
    List<CommentModel> commentModelList;
    CommentAdapter commentAdapter;


    public CommentPresenter(CommentContract.view commentViewView) {
        this.commentViewView = commentViewView;
        this.commentServiceApi = new CommentServiceApiImpl();

    }

    @Override
    public void getComment(String videoId, String token) {
        new viewCommentAsy(videoId, token).execute();
    }

    @Override
    public CommentAdapter getCommentAdapter(Context context, String userId) {
        commentAdapter = new CommentAdapter(commentModelList, context, userId, commentViewView);
        commentAdapter.notifyDataSetChanged();
        return commentAdapter;
    }

    @Override
    public void showWriteComment() {
        commentViewView.writeCommentDialog();
    }

    @Override
    public void writeComment(String videoId, String comment, String token) {
        new writeCommentAsy(videoId, comment, token).execute();
    }

    @Override
    public void showEditComment(String commentId, String comment, int position) {
        commentViewView.editCommentDialog(commentId, comment, position);
    }

    @Override
    public void showDeleteComment(String commentId, int position) {
        commentViewView.deleteCommentDialog(commentId, position);
    }

    @Override
    public void editComment(String commentId, String comment, String token, int position) {
        new editCommentAsy(commentId, comment, token, position).execute();
        commentViewView.cancelDialog();
    }

    @Override
    public void deleteComment(String commentId, String token, int position) {
        new deleteCommentAsy(commentId, token, position).execute();
    }

    class viewCommentAsy extends AsyncTask<String, Integer, String> {
        String videoId;
        String token;

        public viewCommentAsy(String videoId, String token) {
            this.videoId = videoId;
            this.token = token;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            commentViewView.setCommentProgressVisible();
            commentModelList = new ArrayList<>();
        }

        @Override
        protected String doInBackground(String... params) {
            commentServiceApi.viewComment(videoId, token, new CommentServiceApi.CommentServiceCallback<CommentModel>() {
                @Override
                public void onLoaded(CommentModel message) {
                    commentModelList.add(message);
                    commentAdapter.notifyDataSetChanged();
                }
            }, new CommentServiceApi.CommentServiceCallback<String>() {
                @Override
                public void onLoaded(String message) {
                    Log.d("message view", message);
                    if (message.equals("!empty")) {
                        commentViewView.setCommentNullGone();
                    } else if (message.equals("empty")) {
                        commentViewView.setCommentNullVisible();
                    }
                    commentViewView.setCommentProgresGone();

                }
            });
            return "All Done!";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    class writeCommentAsy extends AsyncTask<String, Integer, String> {

        String videoId;
        String comment;
        String token;

        public writeCommentAsy(String videoId, String comment, String token) {
            this.videoId = videoId;
            this.comment = comment;
            this.token = token;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            commentServiceApi.writeComment(videoId, comment, token, new CommentServiceApi.CommentServiceCallback<String>() {
                @Override
                public void onLoaded(String message) {
                    commentViewView.cancelDialog();
                    if (message.equals("Comment saved.")) {
                        commentViewView.setCommentAdapter();
                    }
                    commentViewView.setToast(message);
                }
            });
            return "All Done!";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    class deleteCommentAsy extends AsyncTask<String, Integer, String> {

        String commentId;
        String token;
        int position;

        public deleteCommentAsy(String commentId, String token, int position) {
            this.commentId = commentId;
            this.token = token;
            this.position = position;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            commentServiceApi.deleteComment(commentId, token, new CommentServiceApi.CommentServiceCallback<String>() {
                @Override
                public void onLoaded(String message) {
                    Log.d("message delete", message);
                    if (message.equals("Comment deleted")) {
                        System.out.println("masuk delete true");
                        commentModelList.remove(position);
                        commentAdapter.notifyDataSetChanged();
                        if (commentModelList.size() < 1) {
                            commentViewView.setCommentNullVisible();
                        }
                    }
                    commentViewView.setToast(message);
                }
            });
            return "All Done!";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    class editCommentAsy extends AsyncTask<String, Integer, String> {

        String commentId;
        String comment;
        String token;
        int position;

        public editCommentAsy(String commentId, String comment, String token, int position) {
            this.commentId = commentId;
            this.comment = comment;
            this.token = token;
            this.position = position;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            commentServiceApi.editComment(commentId, comment, token, new CommentServiceApi.CommentServiceCallback<String>() {
                @Override
                public void onLoaded(String message) {
                    Log.d("message edit", message);
                    if (message.equals("Comment edited")) {
                        System.out.println("masuk edit true");
                        commentModelList.get(position).setComment(comment);
                        commentAdapter.notifyDataSetChanged();
                    }
                    commentViewView.setToast(message);
                }
            });
            return "All Done!";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }
}
