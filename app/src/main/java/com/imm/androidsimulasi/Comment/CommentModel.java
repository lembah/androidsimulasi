package com.imm.androidsimulasi.Comment;

import java.util.List;

/**
 * Created by lembah8 on 11/22/16.
 */

public class CommentModel {
    int user_id;
    String comment_id;
    String user_name;
    String profil_pic;
    String comment;
    String create_at;

    public List<CommentModel> commentModelList;

    public CommentModel(){

    }

    public void setUser_id(int id){
        this.user_id = id;
    }

    public int getUser_id(){
        return user_id;
    }

    public void setComment_id(String id){
        this.comment_id = id;
    }

    public String getComment_id(){
        return comment_id;
    }

    public void setUser_name(String name){
        this.user_name = name;
    }

    public String getUser_name(){
        return user_name;
    }

    public void setProfil_pic(String profil_pic){
        this.profil_pic = profil_pic;
    }

    public String getProfil_pic(){
        return profil_pic;
    }

    public void setComment(String comment){
        this.comment = comment;
    }

    public String getComment(){
        return  comment;
    }

    public void setCreate_at(String create_at){
        this.create_at = create_at;
    }

    public String getCreate_at(){
        return create_at;
    }
}
