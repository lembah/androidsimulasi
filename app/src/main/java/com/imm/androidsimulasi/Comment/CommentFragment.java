package com.imm.androidsimulasi.Comment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.imm.androidsimulasi.R;
import com.imm.androidsimulasi.util.ExpandableListView;


public class CommentFragment extends Fragment implements CommentContract.view {

    // Inisialisasi commentPresenter
    CommentContract.actionListener commentViewListener;

    // Inisialisasi vaiabel
    String videoId;
    String token;
    String userId;

    // Inisialisasi class adapter
    CommentAdapter commentAdapter;


    // Inisialisasi comment layout
    private ProgressBar pbarComment;
    private TextView txtCommentNull;
    private ExpandableListView eListViewcomment;
    private TextView txtWriteCOmment;

    // Inisialisasi comment dialog
    private Dialog dialog;
    private Button btnComment;
    private EditText edtComment;

    public CommentFragment(String videoId, String token, String userId) {
        this.videoId = videoId;
        this.token = token;
        this.userId = userId;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        commentViewListener = new CommentPresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_comment, container, false);
        pbarComment = (ProgressBar) rootView.findViewById(R.id.prog_detail_komentar);
        txtCommentNull = (TextView) rootView.findViewById(R.id.txt_detail_comment_null);
        eListViewcomment = (ExpandableListView) rootView.findViewById(R.id.listComment);
        txtWriteCOmment = (TextView) rootView.findViewById(R.id.txt_write_comment);

        setCommentNullGone();

        txtWriteCOmment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commentViewListener.showWriteComment();
            }
        });

        // set Comment Adapter
        setCommentAdapter();

        return rootView;
    }

    @Override
    public void setCommentAdapter(){
        commentViewListener.getComment(videoId, token);
        commentAdapter = commentViewListener.getCommentAdapter(getContext(), userId);
        eListViewcomment.setAdapter(commentAdapter);
        commentAdapter.notifyDataSetChanged();
        eListViewcomment.setExpanded(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setCommentProgressVisible() {
        pbarComment.setVisibility(ViewGroup.VISIBLE);
    }

    @Override
    public void setCommentProgresGone() {
        pbarComment.setVisibility(ViewGroup.GONE);
    }

    @Override
    public void setCommentNullVisible() {
        txtCommentNull.setVisibility(ViewGroup.VISIBLE);
    }

    @Override
    public void setCommentNullGone() {
        txtCommentNull.setVisibility(ViewGroup.GONE);
    }

    public interface OnFragmentInteractionListener {
    }

    @Override
    public void writeCommentDialog(){
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_edit_comment);

        btnComment = (Button) dialog.findViewById(R.id.btn_edit_do_comment);
        edtComment = (EditText) dialog.findViewById(R.id.edt_edit_comment);

        btnComment.setEnabled(isEmpty(edtComment.getText().toString()));
        edtComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                btnComment.setEnabled(isEmpty(edtComment.getText().toString()));
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        Window dialogWindow = dialog.getWindow();
        dialogWindow.setGravity(Gravity.BOTTOM);
        dialogWindow.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();

        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commentViewListener.writeComment(videoId, edtComment.getText().toString(), token);
                // call listener to write to comment
            }
        });
    }

    @Override
    public void editCommentDialog(final String commentId, String comment, final int position) {
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_edit_comment);

        btnComment = (Button) dialog.findViewById(R.id.btn_edit_do_comment);
        edtComment = (EditText) dialog.findViewById(R.id.edt_edit_comment);

        edtComment.setText(comment);
        btnComment.setEnabled(isEmpty(edtComment.getText().toString()));
        edtComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                btnComment.setEnabled(isEmpty(edtComment.getText().toString()));
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        Window dialogWindow = dialog.getWindow();
        dialogWindow.setGravity(Gravity.BOTTOM);
        dialogWindow.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();

        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commentViewListener.editComment(commentId, edtComment.getText().toString(), token, position);
                // call listener to write to comment
            }
        });
    }

    @Override
    public void deleteCommentDialog(final String commentId, final int position){
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setTitle("Delete Comment");
        alert.setMessage("did yout want to delete this comment ?");
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                System.out.println("delete yes");
                commentViewListener.deleteComment(commentId, token, position);
                // call Listener to delete comment
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {}
        });
        alert.show();
    }

    @Override
    public void setToast(String message){
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void cancelDialog(){
        dialog.dismiss();
    }

    boolean isEmpty(String comment) {
        return !comment.isEmpty();
    }

}
