package com.imm.androidsimulasi.Comment;

import android.content.Context;

/**
 * Created by lembah8 on 12/6/16.
 */

public interface CommentContract {
    interface view{
        void setCommentAdapter();

        void setCommentProgressVisible();

        void setCommentProgresGone();

        void setCommentNullVisible();

        void setCommentNullGone();

        void setToast(String message);

        void writeCommentDialog();

        void editCommentDialog(String commentId, String comment, int position);

        void deleteCommentDialog(String commentId, int position);

        void cancelDialog();

    }
    interface actionListener{
        void getComment(String videoId, String token);

        void showWriteComment();

        void writeComment(String videoId, String comment, String token);

        void showEditComment(String commentId, String comment, int position);

        void editComment(String commentId, String comment, String token, int position);

        void showDeleteComment(String commentId, int position);

        void deleteComment(String commentId, String token, int position);

        CommentAdapter getCommentAdapter(Context context, String userId);
    }
}
