package com.imm.androidsimulasi.Comment;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.imm.androidsimulasi.R;

import java.util.List;

/**
 * Created by lembah8 on 11/22/16.
 */

public class CommentAdapter extends ArrayAdapter<CommentModel> {

    private List<CommentModel> commentItems;
    //    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private Context mContext;
    private String userId;
    CommentContract.actionListener commentViewListener;

    public CommentAdapter(List<CommentModel> commentItems, Context context, String userId, CommentContract.view commentViewView) {
        super(context, R.layout.list_row_comment);
        this.commentItems = commentItems;
        this.mContext = context;
        this.userId = userId;
        this.commentViewListener = new CommentPresenter(commentViewView);

    }

    @Override
    public int getCount() {
        return commentItems.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from((getContext()));
        convertView = inflater.inflate(R.layout.list_row_comment, parent, false);
        final TextView comment = (TextView) convertView.findViewById(R.id.txt_detail_komentar_text);
        TextView user = (TextView) convertView.findViewById(R.id.txt_detail_komentar_user);
        TextView create_at = (TextView) convertView.findViewById(R.id.txt_detail_komentar_date);
        final ImageView optionMenu = (ImageView) convertView.findViewById(R.id.img_detail_option_comment);

        final CommentModel m = commentItems.get(position);

        comment.setText(m.getComment());

        user.setText(m.getUser_name());

        create_at.setText(m.getCreate_at() + " ago");

        optionMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenuComment = new PopupMenu(mContext, optionMenu);
                System.out.println("userId"+userId);
                System.out.println("m.getUser_id"+m.getUser_id());
                if (userId.equals(String.valueOf(m.getUser_id()))) {
                    popupMenuComment.getMenuInflater().inflate(R.menu.menu_user_own_option, popupMenuComment.getMenu());
                } else {
                    popupMenuComment.getMenuInflater().inflate(R.menu.menu_user_general_option, popupMenuComment.getMenu());
                }
                popupMenuComment.setGravity(Gravity.BOTTOM);

                popupMenuComment.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().equals(getContext().getString(R.string.option_delete))) {
                            commentViewListener.showDeleteComment(m.getComment_id(), position);
//                            commentViewListener.deleteComment(commentId, token);
                            System.out.println("ini delete");
                        }
                        if (item.getTitle().equals(getContext().getString(R.string.option_edit))) {
                            commentViewListener.showEditComment(m.getComment_id(), m.getComment(), position);
                            System.out.println("ini edit");
                        }
                        if (item.getTitle().equals(getContext().getString(R.string.option_report))) {
                            System.out.println("ini report");
                        }
                        return false;
                    }
                });
                popupMenuComment.show();
            }
        });

        return convertView;
    }
}

