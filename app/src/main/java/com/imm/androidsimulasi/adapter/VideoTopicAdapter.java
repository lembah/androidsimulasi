package com.imm.androidsimulasi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.imm.androidsimulasi.R;
import com.imm.androidsimulasi.model.TimelineModel;
import com.imm.androidsimulasi.util.AppController;

import java.util.List;

/**
 * Created by lembah8 on 11/23/16.
 */

public class VideoTopicAdapter extends ArrayAdapter<TimelineModel> {

    private LayoutInflater inflater;
    private List<TimelineModel> videoTopicItems;
    private Context mContext;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    public VideoTopicAdapter(List<TimelineModel> videoTopicItems, Context context) {
        super(context, R.layout.list_row_comment);
        this.videoTopicItems = videoTopicItems;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return videoTopicItems.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        inflater = LayoutInflater.from((getContext()));
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_video_topic, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        TextView title = (TextView) convertView.findViewById(R.id.txt_videoTopic_title);
        TextView user = (TextView) convertView.findViewById(R.id.txt_videoTopic_user);
        TextView create_at = (TextView) convertView.findViewById(R.id.txt_videoTopic_date);
        ImageView thumbnail = (ImageView) convertView.findViewById(R.id.img_videoTopic_thumb);

        // getting comment data for the row
        TimelineModel m = videoTopicItems.get(position);

        // user profil pic
        Glide.with(mContext).load(m.getVideo_thumbnail())
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(thumbnail);

        // comment
        title.setText(m.getTitle());

        // user name
        user.setText(m.getUploader());

        // create at
        create_at.setText(m.getCreated_at()+" ago");


        return convertView;
    }
}
