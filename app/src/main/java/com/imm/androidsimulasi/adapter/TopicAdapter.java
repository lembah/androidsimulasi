package com.imm.androidsimulasi.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.imm.androidsimulasi.R;
import com.imm.androidsimulasi.model.TopicModel;
import com.imm.androidsimulasi.util.AppController;

import java.util.List;

/**
 * Created by lembah8 on 11/23/16.
 */

public class TopicAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List<TopicModel> topicItems;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private Context mContext;
    public TopicAdapter(Activity activity, List<TopicModel> topicItems, Context context) {
        this.activity = activity;
        this.topicItems = topicItems;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return topicItems.size();
    }

    @Override
    public Object getItem(int location) {
        return topicItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_topic, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        /*NetworkImageView thumbNail = (NetworkImageView) convertView
                .findViewById(R.id.thumbnail);*/

        TextView topic = (TextView) convertView.findViewById(R.id.txt_topic);
        ImageView thumbnail = (ImageView) convertView.findViewById(R.id.img_topic);

        // getting movie data for the row
        TopicModel m = topicItems.get(position);

        // thumbnail image
        //thumbNail.setImageUrl(m.getThumbnailUrl(), imageLoader);
        Glide.with(mContext).load(m.getThumbnail())
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(thumbnail);

        // title
        topic.setText(m.getTopic());
        
        return convertView;
    }
}
