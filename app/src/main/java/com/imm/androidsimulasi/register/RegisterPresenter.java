package com.imm.androidsimulasi.register;

import android.support.annotation.NonNull;

/**
 * Created by lembah8 on 11/30/16.
 */

public class RegisterPresenter implements RegisterContract.actionListener {
    @NonNull
    private final RegisterContract.view registerView;

    public RegisterPresenter(@NonNull RegisterContract.view registerView) {
        this.registerView = registerView;
    }

    @Override
    public void Register() {

        if (registerView.checkFullname() && registerView.checkEmail() && registerView.checkPassword() && registerView.checkRePassword()){
            registerView.doRegister();
        }
    }
}

