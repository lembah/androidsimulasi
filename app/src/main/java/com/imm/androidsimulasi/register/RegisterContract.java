package com.imm.androidsimulasi.register;

/**
 * Created by lembah8 on 11/30/16.
 */

public interface RegisterContract {
    interface view{
        boolean checkFullname();
        boolean checkEmail();
        boolean checkPassword();
        boolean checkRePassword();
        boolean doRegister();
        void initToolbar();
        void clickAbleLogin();
        boolean isEmpty(String test);
        boolean isEmailValid(String email);
        boolean isPasswordValid(String password);
        boolean isRePasswordValid(String password, String rePassword);
    }
    interface actionListener{
        void Register();
    }
}
