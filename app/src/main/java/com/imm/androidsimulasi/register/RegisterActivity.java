package com.imm.androidsimulasi.register;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.imm.androidsimulasi.R;
import com.imm.androidsimulasi.activity.LoginActivity;
import com.imm.androidsimulasi.helper.AppConfig;
import com.imm.androidsimulasi.helper.IntentHelper;
import com.imm.androidsimulasi.helper.SessionManager;
import com.imm.androidsimulasi.util.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity implements RegisterContract.view{

//    private EditText g_fullName;
//    private EditText g_email;
//    private EditText g_password;
//    private EditText g_rePassword;
//    private TextView g_login;
//    private ProgressDialog g_dialog;
//    private User user_data;

    private EditText edtFullName;
    private EditText edtEmail;
    private EditText edtPassword;
    private EditText edtRePassword;

    //SharedPreferences sharedPreferences;
    private SessionManager session;
    private RegisterContract.actionListener actionListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Init Toolbar
        initToolbar();

        //Load ClickAble Login
        clickAbleLogin();

        //sharedPreferences = getSharedPreferences("user_data", Context.MODE_PRIVATE);
        session = new SessionManager(getApplicationContext());

        actionListener = new RegisterPresenter(this);
        //Inisialisasi Input
        edtFullName = (EditText) findViewById(R.id.edt_fullname);
        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        edtRePassword = (EditText) findViewById(R.id.edt_rePassword);
        Button button_Register = (Button) findViewById(R.id.btn_register);
        button_Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actionListener.Register();
//                Register(edtFullName, edtEmail, edtPassword, edtRePassword);
//                attemptRegister();
            }
        });

    }

    @Override
    public boolean checkFullname() {
        edtFullName.setError(null);
        String fullname = edtFullName.getText().toString();
        if(isEmpty(fullname)){
            edtFullName.setError(getString(R.string.error_field_required));
            return false;
        }
        return true;
    }

    @Override
    public boolean checkEmail(){
        edtEmail.setError(null);
        String email = edtEmail.getText().toString();
        if(isEmpty(email)){
            edtEmail.setError(getString(R.string.error_field_required));
            return false;
        }else if(!isEmailValid(email)){
            edtEmail.setError(getString(R.string.error_invalid_email));
            return false;
        }
        return true;
    }

    @Override
    public boolean checkPassword(){
        edtPassword.setError(null);

        String password = edtPassword.getText().toString();
        if(TextUtils.isEmpty(password)){
            edtPassword.setError(getString(R.string.error_field_required));
            return false;
        }else if(isPasswordValid(password)){
            edtPassword.setError(getString(R.string.error_register_short_password));
            return false;
        }
        return true;
    }

    @Override
    public boolean checkRePassword(){
        edtRePassword.setError(null);
        String rePassword = edtRePassword.getText().toString();
        String password = edtPassword.getText().toString();
        if(TextUtils.isEmpty(rePassword)){
            edtRePassword.setError(getString(R.string.error_field_required));
            return false;
        }else if (!isRePasswordValid(password, rePassword)){
            edtRePassword.setError(getString(R.string.error_password_not_same));
            return false;
        }
        return true;
    }

    @Override
    public boolean doRegister() {
        final ProgressDialog  dialog = ProgressDialog.show(RegisterActivity.this,"","Loading...",true);
        String url = AppConfig.URL_REGISTER;
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("name", edtFullName.getText().toString());
        params.put("email", edtEmail.getText().toString());
        params.put("password",edtPassword.getText().toString());
        params.put("password_confirmation",edtPassword.getText().toString());
        JsonObjectRequest req = new JsonObjectRequest(url, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println(response.toString());
                            String error = response.getString("error");
                            if(error.equals("false"))
                            {
                                JSONObject jsonObject = response.getJSONObject("data");
                                String id = jsonObject.getString("id");
                                String name = jsonObject.getString("name");
                                String email = jsonObject.getString("email");
                                String token = jsonObject.getString("token");
                                session.createLoginSession(id, name, email, token);

                                Toast.makeText(getApplicationContext(),  "Registrasion Succesfull", Toast.LENGTH_LONG).show();

                                // Pindah layout
                                /** Enhance intent*/
                                IntentHelper.redirectActivity(RegisterActivity.this, LoginActivity.class);
                                finish();
                            }
                            else{
                                String message = response.getString("message");
                                Toast.makeText(getApplicationContext(),  message, Toast.LENGTH_LONG).show();
                                edtEmail.setError(getString(R.string.error_email_already_registered));
                                View focusView = edtEmail;
                                focusView.requestFocus();
                            }
                            dialog.dismiss();
                        } catch (JSONException e) {
                            dialog.dismiss();
                            Toast.makeText(getApplicationContext(),  "Registrasion Failed", Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(getApplicationContext(),  "Registrasion Failed", Toast.LENGTH_LONG).show();
                error.printStackTrace();
                VolleyLog.e("Error: ", error.getMessage());
            }
        });
        AppController.getInstance().addToRequestQueue(req);
        return true;
    }

    @Override
    public void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_register);
        toolbar.setTitle("Register Account");

        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void clickAbleLogin()
    {
        SpannableString stringLogin = new SpannableString(getString(R.string.span_login));
        ClickableSpan spanLogin = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        };

        stringLogin.setSpan(spanLogin, 24, 29, 0);
        TextView gLogin = (TextView) findViewById(R.id.txt_spanToLogin);
        gLogin.setMovementMethod(LinkMovementMethod.getInstance());
        gLogin.setText(stringLogin);
    }

    @Override
    public boolean isEmpty(String string) {
        return string.isEmpty();
    }

    @Override
    public boolean isEmailValid(String email) {
        return email.contains("@") && !email.contains(" ");
    }

    @Override
    public boolean isPasswordValid(String password) {
        return password.length() < 7;
    }

    @Override
    public boolean isRePasswordValid(String password, String rePassword) {
        return rePassword.equals(password);
    }


//    private void attemptRegister()
//    {
//        //initialize
//        g_fullName.setError(null);
//        g_email.setError(null);
//        g_password.setError(null);
//        g_rePassword.setError(null);
//
//        final String l_fullname = g_fullName.getText().toString();
//        final String l_email = g_email.getText().toString();
//        final String l_password = g_password.getText().toString();
//        final String l_rePassword = g_rePassword.getText().toString();
//
//
//        boolean cancel = false;
//        View focusView = null;
//
//        if(TextUtils.isEmpty(l_fullname))
//        {
//            g_fullName.setError(getString(R.string.error_field_required));
//            focusView = g_fullName;
//            cancel = true;
//        }
//
//        if(TextUtils.isEmpty(l_email))
//        {
//            g_email.setError(getString(R.string.error_field_required));
//            focusView = g_email;
//            cancel = true;
//        }else if(!isEmailValid(l_email))
//        {
//            g_email.setError(getString(R.string.error_invalid_email));
//            focusView = g_email;
//            cancel = true;
//        }
//
//
//        if(TextUtils.isEmpty(l_password))
//        {
//            g_password.setError(getString(R.string.error_field_required));
//            focusView = g_password;
//            cancel = true;
//        }else if(!isPasswordTooShort(l_password))
//        {
//            g_password.setError(getString(R.string.error_register_short_password));
//            focusView = g_password;
//            cancel = true;
//        }
//
//        if(TextUtils.isEmpty(l_rePassword))
//        {
//            g_rePassword.setError(getString(R.string.error_field_required));
//            focusView = g_rePassword;
//            cancel = true;
//        }else if(!isPasswordValid(l_password, l_rePassword))
//        {
//            g_rePassword.setError(getString(R.string.error_password_not_same));
//            focusView = g_rePassword;
//            cancel = true;
//        }
//
//        if(cancel)
//        {
//            focusView.requestFocus();
//        }else
//        {
//            doRegiter(l_fullname, l_email, l_password);
//        }
//
//    }
//
//    private boolean isPasswordValid(String password, String rePassword)
//    {
//        return password.equals(rePassword);
//    }
//
//    private boolean isPasswordTooShort(String password)
//    {
//        return password.length() > 7;
//    }
//
//    private boolean isEmailValid(String email){
//        return email.contains("@");
//    }


    //membuat clickable  span untuk ke halaman login


//
//    private void doRegiter(String fullname, String email, String password)
//    {
//        String url = AppConfig.URL_REGISTER;
//        HashMap<String, String> params = new HashMap<String, String>();
//        params.put("name", fullname);
//        params.put("email", email);
//        params.put("password",password);
//        params.put("password_confirmation",password);
//
//        g_dialog = ProgressDialog.show(RegisterActivity.this,"","Loading...",true);
//
//        JsonObjectRequest req = new JsonObjectRequest(url, new JSONObject(params),
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            System.out.println(response.toString());
//                            String error = response.getString("error");
//                            System.out.println(error);
//                            if(error.equals("false"))
//                            {
//                                JSONObject jsonObject = response.getJSONObject("data");
//                                String id = jsonObject.getString("id");
//                                String name = jsonObject.getString("name");
//                                String email = jsonObject.getString("email");
//                                String token = jsonObject.getString("token");
//
//
//                                /*System.out.println(name);
//                                System.out.println(email);
//                                System.out.println(token);*/
//                                /** Use log to see result*/
//                                Log.e("name : ", name);
//                                Log.e("email : ", email);
//                                Log.e("toke : ", token);
//
//
//                                // Simpan token ke session
//                                /*SharedPreferences.Editor editor = sharedPreferences.edit();
//                                editor.putString("name", name);
//                                editor.putString("email", email);
//                                editor.putString("token", token);
//                                editor.commit();*/
//                                /** Edited by firhat simplify session manager */
//                                session.createLoginSession(id, name, email, token);
//
//
//                                Toast.makeText(getApplicationContext(),  "Registrasion Succesfull", Toast.LENGTH_LONG).show();
//
//
//                                // Pindah layout
//                                /*Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
//                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                startActivity(intent);*/
//                                /** Enhance intent*/
//                                IntentHelper.redirectActivity(RegisterActivity.this, LoginActivity.class);
//                                finish();
//                            }
//                            else{
//                                String message = response.getString("message");
//                                Toast.makeText(getApplicationContext(),  message, Toast.LENGTH_LONG).show();
//                                g_email.setError(getString(R.string.error_email_already_registered));
//                                View focusView = g_email;
//                                focusView.requestFocus();
//                            }
//                            g_dialog.dismiss();
//                            VolleyLog.v("Response:%n %s", response.toString(4));
//                        } catch (JSONException e) {
//                            g_dialog.dismiss();
//                            e.printStackTrace();
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                g_dialog.dismiss();
//                Toast.makeText(getApplicationContext(),  "Registrasion Failed", Toast.LENGTH_LONG).show();
//                error.printStackTrace();
//                VolleyLog.e("Error: ", error.getMessage());
//            }
//        });
//        AppController.getInstance().addToRequestQueue(req);
//    }

}
