package com.imm.androidsimulasi.LikeDislikeVideo;

/**
 * Created by lembah8 on 12/6/16.
 */

public interface LikeDislikeContract {
    interface view{
        void setToast(String message);
    }
    interface actionListener{
        void doLike(String videoId, String token);
        void doDislike(String videoId, String token);
    }
}
