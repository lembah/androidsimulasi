package com.imm.androidsimulasi.LikeDislikeVideo;

import com.imm.androidsimulasi.Data.LikeDislikeServiceApiImpl;
import com.imm.androidsimulasi.Data.LikeDislikeServideApi;

/**
 * Created by lembah8 on 12/6/16.
 */

public class LikeDisLikePresenter implements LikeDislikeContract.actionListener {

    LikeDislikeContract.view likeDislikeview;
    private final LikeDislikeServideApi likeDislikeServideApi;


    public LikeDisLikePresenter(LikeDislikeContract.view likeDislikeview) {
        this.likeDislikeview = likeDislikeview;
        this.likeDislikeServideApi = new LikeDislikeServiceApiImpl();
    }

    @Override
    public void doLike(String videoId, String token) {
        likeDislikeServideApi.setLike(videoId, token, new LikeDislikeServideApi.LikeDislikeServiceCallback<String>() {
            @Override
            public void onLoaded(String message) {
                likeDislikeview.setToast(message);
            }
        });
    }

    @Override
    public void doDislike(String videoId, String token) {
        likeDislikeServideApi.setDislike(videoId, token, new LikeDislikeServideApi.LikeDislikeServiceCallback<String>() {
            @Override
            public void onLoaded(String message) {
                likeDislikeview.setToast(message);
            }
        });
    }
}
