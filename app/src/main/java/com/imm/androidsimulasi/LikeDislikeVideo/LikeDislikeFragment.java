package com.imm.androidsimulasi.LikeDislikeVideo;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.imm.androidsimulasi.R;

public class LikeDislikeFragment extends Fragment implements LikeDislikeContract.view{

    ImageView imgLike;
    ImageView imgDislike;
    TextView txtLike;
    TextView txtDislike;

    Boolean like = false;
    Boolean disLike = false;
    String likeCount;
    String dislikeCount;
    String videoId;
    String token;

    LikeDislikeContract.actionListener likeDislikeListener;

    public LikeDislikeFragment(String likeCount, String disLikeCount, String videoId, String token) {
        this.likeCount = likeCount;
        this.dislikeCount = disLikeCount;
        this.videoId = videoId;
        this.token = token;
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.likeDislikeListener = new LikeDisLikePresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_like_dislike, container, false);
        imgLike = (ImageView) rootView.findViewById(R.id.img_detail_like);
        imgDislike = (ImageView) rootView.findViewById(R.id.img_detail_unlike);
        txtLike = (TextView) rootView.findViewById(R.id.txt_detail_like);
        txtDislike = (TextView) rootView.findViewById(R.id.txt_detail_unlike);

        txtLike.setText(likeCount);
        txtDislike.setText(dislikeCount);

        imgDislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                disLike = !disLike;
                likeDislikeListener.doDislike(videoId, token);
                if (disLike) {
                    if (like) {
                        like = false;
                        imgDislike.setColorFilter(getResources().getColor(R.color.dot_dark_screen1));
                        imgLike.setColorFilter(getResources().getColor(R.color.navigationBarColor));
                        likeCount = String.valueOf(Integer.parseInt(likeCount) - 1);
                        dislikeCount = String.valueOf(Integer.parseInt(dislikeCount) + 1);
                        txtLike.setText(likeCount);
                        txtDislike.setText(dislikeCount);
                    } else {
                        imgDislike.setColorFilter(getResources().getColor(R.color.dot_dark_screen1));
                        dislikeCount = String.valueOf(Integer.parseInt(dislikeCount) + 1);
                        txtDislike.setText(dislikeCount);
                    }
                } else {
                    imgDislike.setColorFilter(getResources().getColor(R.color.navigationBarColor));
                    dislikeCount = String.valueOf(Integer.parseInt(dislikeCount) - 1);
                    txtDislike.setText(dislikeCount);
                }
            }
        });

        imgLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                like = !like;
                likeDislikeListener.doLike(videoId,token);
                if (like) {
                    if (disLike) {
                        disLike = false;
                        imgLike.setColorFilter(getResources().getColor(R.color.dot_dark_screen1));
                        imgDislike.setColorFilter(getResources().getColor(R.color.navigationBarColor));
                        likeCount = String.valueOf(Integer.parseInt(likeCount) + 1);
                        dislikeCount = String.valueOf(Integer.parseInt(dislikeCount) - 1);
                        txtLike.setText(likeCount);
                        txtDislike.setText(dislikeCount);
                    } else {
                        imgLike.setColorFilter(getResources().getColor(R.color.dot_dark_screen1));
                        likeCount = String.valueOf(Integer.parseInt(likeCount) + 1);
                        txtLike.setText(likeCount);
                    }
                } else {
                    imgLike.setColorFilter(getResources().getColor(R.color.navigationBarColor));
                    likeCount = String.valueOf(Integer.parseInt(likeCount) - 1);
                    txtLike.setText(likeCount);
                }
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setToast(String message){
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    public interface OnFragmentInteractionListener {
    }
}
