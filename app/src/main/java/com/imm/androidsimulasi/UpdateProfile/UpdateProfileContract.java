package com.imm.androidsimulasi.UpdateProfile;

import android.content.Context;
import android.net.Uri;

import com.imm.androidsimulasi.model.UpdateProfileModel;

/**
 * Created by lembah8 on 12/8/16.
 */

public interface UpdateProfileContract {
    interface view{
        void setDialogShow();

        void setDialogCancel();

        void setToast(String message);

        void setTxtUserFullname(String fullname);

        void setEdtUserFullname(String fullname);

        void setImgProfilPic(String imgUrl);

        boolean checkFullname();

    }
    interface actionListener{
        void viewProfile(String userId, String token, String userAgent);

        void updateProfilePic(Uri imgUri, String token, String userAgent);

        void updateProfile(String fullname, String userId, String token, String userAgent);
    }
}
