package com.imm.androidsimulasi.UpdateProfile;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.imm.androidsimulasi.R;
import com.imm.androidsimulasi.helper.AppConfig;
import com.imm.androidsimulasi.helper.ProgressDialogCustom;
import com.imm.androidsimulasi.helper.SessionManager;
import com.imm.androidsimulasi.model.VideoModel;
import com.imm.androidsimulasi.util.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * TODO :
 * profile pict waiting in api
 */

public class UpdateProfileActivity extends AppCompatActivity implements UpdateProfileContract.view {

    // Progress dialog
    private ProgressDialogCustom pDialog;

    private Toolbar mToolbar;
    TextView txtFullname;
    EditText edtFullname;
    ImageView imgProfilPic;
    RelativeLayout layoutProfilPic;

    Dialog dialogUpdatePic;

    String userId;
    String token;
    String userAgent;

    UpdateProfileContract.actionListener updateProfilListener;

    static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int SELECT_PICTURE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        mToolbar = (Toolbar) findViewById(R.id.toolbar_update_profile);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtFullname = (TextView) findViewById(R.id.txt_update_user_fullname);
        edtFullname = (EditText) findViewById(R.id.edt_update_user_fullname);
        imgProfilPic = (ImageView) findViewById(R.id.img_update_profil_pict);
        layoutProfilPic = (RelativeLayout) findViewById(R.id.layout_update_user_profil_pic);

        // get session element
        token = SessionManager.getKeyToken(this);
        userId = SessionManager.getUserId(this);
        userAgent = System.getProperty("http.agent");
        Log.e("TOKEN", token);
        Log.e("AGENT", userAgent);

        // Initiate dialog
        pDialog = new ProgressDialogCustom(this);

        updateProfilListener = new UpdateProfilePresenter(this);

        updateProfilListener.viewProfile(userId, token, userAgent);

        layoutProfilPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProfilePicDialog();
            }
        });

    }

    @Override
    public void setDialogShow() {
        pDialog.showpDialog();
    }

    @Override
    public void setDialogCancel() {
        pDialog.hidepDialog();
    }

    @Override
    public void setToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setTxtUserFullname(String fullname) {
        txtFullname.setText(fullname);
    }

    @Override
    public void setEdtUserFullname(String fullname) {
        edtFullname.setText(fullname);
    }

    @Override
    public void setImgProfilPic(String imgUrl) {
        ImageRequest request = new ImageRequest(imgUrl,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        imgProfilPic.setImageBitmap(bitmap);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        imgProfilPic.setImageResource(R.drawable.sample_pro_pict);
                    }
                });
        AppController.getInstance().addToRequestQueue(request);
    }

    public void updateProfilePicDialog() {
        dialogUpdatePic = new Dialog(this);
        dialogUpdatePic.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogUpdatePic.setContentView(R.layout.dialog_update_profil_pic);

        final TextView choosePic = (TextView) dialogUpdatePic.findViewById(R.id.btn_pilih_foto);
        final TextView takePic = (TextView) dialogUpdatePic.findViewById(R.id.btn_ambil_foto);
        final TextView deletePic = (TextView) dialogUpdatePic.findViewById(R.id.btn_hapus_foto);

        choosePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
            }
        });

        Window dialogWindow = dialogUpdatePic.getWindow();
        dialogWindow.setGravity(Gravity.CENTER);
        dialogWindow.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogUpdatePic.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK) {
            Uri imageUri = intent.getData();
            dialogUpdatePic.dismiss();
            updateProfilListener.updateProfilePic(imageUri, token, userAgent);
            try {
                Bitmap imageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                imgProfilPic.setImageBitmap(imageBitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public boolean checkFullname(){
        edtFullname.setError(null);
        String fullname = edtFullname.getText().toString();
        if(fullname.isEmpty()){
            edtFullname.setError(getString(R.string.error_field_required));
            return false;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.save_profile) {
            updateProfilListener.updateProfile(edtFullname.getText().toString(), userId, token, userAgent);
//            updateProfile();
            return true;
        }

        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
