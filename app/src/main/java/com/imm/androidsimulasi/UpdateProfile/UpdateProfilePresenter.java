package com.imm.androidsimulasi.UpdateProfile;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.imm.androidsimulasi.Data.ProfileServiceApi;
import com.imm.androidsimulasi.Data.ProfileServiceApiImpl;
import com.imm.androidsimulasi.activity.MainActivity;
import com.imm.androidsimulasi.activity.UploadVideoActivity;
import com.imm.androidsimulasi.helper.AppConfig;
import com.imm.androidsimulasi.helper.IntentHelper;
import com.imm.androidsimulasi.helper.MultipartHelper;
import com.imm.androidsimulasi.model.UpdateProfileModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

/**
 * Created by lembah8 on 12/8/16.
 */

public class UpdateProfilePresenter implements UpdateProfileContract.actionListener {

    UpdateProfileContract.view updateProfileView;
    ProfileServiceApi profileServiceApi;

    public UpdateProfilePresenter(UpdateProfileContract.view updateProfileView) {
        this.updateProfileView = updateProfileView;
        this.profileServiceApi = new ProfileServiceApiImpl();
    }


    @Override
    public void viewProfile(String userId, String token, String userAgent) {
        updateProfileView.setDialogShow();
        profileServiceApi.viewProfile(userId, token, userAgent, new ProfileServiceApi.ProfileServiceCallback<UpdateProfileModel>() {
            @Override
            public void onLoaded(UpdateProfileModel message) {
                updateProfileView.setTxtUserFullname(message.getUserFullname());
                updateProfileView.setEdtUserFullname(message.getUserFullname());
                updateProfileView.setImgProfilPic(message.getUserPic());
            }
        }, new ProfileServiceApi.ProfileServiceCallback<String>() {
            @Override
            public void onLoaded(String message) {
                if (message.equals("true")) {
                    updateProfileView.setDialogCancel();
                } else if (message.equals("false")) {
                    updateProfileView.setDialogCancel();
                    updateProfileView.setToast("no profile availabel");
                } else {
                    updateProfileView.setDialogCancel();
                    updateProfileView.setToast(message);
                }
            }
        });
    }

    @Override
    public void updateProfilePic(final Uri imgUri, final String token, final String userAgent) {
        updateProfileView.setDialogShow();
        profileServiceApi.updateProfilPic(imgUri, token, userAgent, new ProfileServiceApi.ProfileServiceCallback<List<String>>() {
            @Override
            public void onLoaded(List<String> message) {
                updateProfileView.setDialogCancel();
                if(message.get(0).equals("false"))
                {
                    updateProfileView.setToast(message.get(1));
                }else if(message.get(0).equals("true")){
                    updateProfileView.setImgProfilPic(message.get(1));
                }
            }
        });
    }

    @Override
    public void updateProfile(final String fullname, String userId, String token, String userAgent) {
        updateProfileView.setDialogShow();
        if(updateProfileView.checkFullname()){
            profileServiceApi.UpdateProfile(fullname, userId, token, userAgent, new ProfileServiceApi.ProfileServiceCallback<String>() {
                @Override
                public void onLoaded(String message) {
                    updateProfileView.setToast(message);
                    if(message.equals("Name has been updated")){
                        updateProfileView.setTxtUserFullname(fullname);
                        updateProfileView.setEdtUserFullname(fullname);
                    }
                    updateProfileView.setDialogCancel();
                }
            });
        }
    }

}
