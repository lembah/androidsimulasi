package com.imm.androidsimulasi.ViewVideoDetail;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.imm.androidsimulasi.Comment.CommentFragment;
import com.imm.androidsimulasi.LikeDislikeVideo.LikeDislikeFragment;
import com.imm.androidsimulasi.R;
import com.imm.androidsimulasi.UserActionVideo.UserActionVideoFragment;
import com.imm.androidsimulasi.VideoDetail.VideoDetailFragment;
import com.imm.androidsimulasi.VideoShare.VideoShareFragment;
import com.imm.androidsimulasi.VideoView.VideoViewFragment;
import com.imm.androidsimulasi.helper.SessionManager;

import java.util.HashMap;


public class ViewVideoDetailActivity extends AppCompatActivity {

    private SessionManager session;
    private String token;
    private String userId;

    private String gVideoId;
    private String gUserId;
    private String gVideoTitle;
    private String gVideoTopic;
    private String gVideoURL;
    private String gVideoDesc;
    private String gVideoLike;
    private String gVideoUnLike;
    private String gVideoUploader;
    private String gVideoCreateDate;

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_video_detail);

        initToolbar();

        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        token = user.get(SessionManager.KEY_TOKEN);
        userId = user.get(SessionManager.KEY_ID);

        // Get Intent Extra
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            gVideoId = extras.getString("video_id");
            gUserId = extras.getString("user_id");
            gVideoTitle = extras.getString("title");
            gVideoTopic = extras.getString("topic");
            gVideoURL = extras.getString("video");
            gVideoDesc = extras.getString("desc");
            gVideoLike = extras.getString("like");
            gVideoUnLike = extras.getString("unlike");
            gVideoUploader = extras.getString("uploader");
            gVideoCreateDate = extras.getString("create_date");
        }

        Fragment videoViewFragment = new VideoViewFragment(this, gVideoURL, gUserId, userId);
        setFragmentVideoView(videoViewFragment);

        Fragment videoDetailFragment = new VideoDetailFragment(gVideoTitle, gVideoTopic, gVideoDesc, gVideoUploader, gVideoCreateDate);
        setFragmentVideoDetail(videoDetailFragment);

        Fragment userActionVideoFragment = new UserActionVideoFragment(gVideoId, userId, gUserId, token, this);
        setFragmentUserActionVideo(userActionVideoFragment);

        Fragment likeDislikeFragment = new LikeDislikeFragment(gVideoLike, gVideoUnLike, gVideoId, token);
        setFragmentLikeDislike(likeDislikeFragment);

        Fragment videoShareFragment = new VideoShareFragment();
        setFragmentVideoShare(videoShareFragment);

        Fragment commentFragment = new CommentFragment(gVideoId, token, userId);
        setFragmentComment(commentFragment);
    }

    void setFragmentVideoView(Fragment fragment)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.layout_video_view, fragment);
        fragmentTransaction.commit();
    }

    void setFragmentVideoDetail(Fragment fragment)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.layout_video_detail, fragment);
        fragmentTransaction.commit();
    }

    void setFragmentUserActionVideo(Fragment fragment)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.layout_user_action_video, fragment);
        fragmentTransaction.commit();
    }

    void setFragmentLikeDislike(Fragment fragment)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.layout_like_dislike, fragment);
        fragmentTransaction.commit();
    }

    void setFragmentVideoShare(Fragment fragment)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.layout_share_video, fragment);
        fragmentTransaction.commit();
    }

    void setFragmentComment(Fragment fragment)
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.layout_comment, fragment);
        fragmentTransaction.commit();
    }


    void initToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar_video_view_detail);
        toolbar.setTitle("Video Detail");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
