package com.imm.androidsimulasi.VideoEdit;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import com.imm.androidsimulasi.R;

import com.imm.androidsimulasi.util.CustomTimeLineView;

public class VideoThumbnailFragment extends Fragment {

    private VideoEditActivity videoEditActivity;
    private SeekBar mHolderTopView;
    private CustomTimeLineView mTimeLineView;

    private int mDuration;
    private int mStartDuration;
    private int mEndDuration;

    public VideoThumbnailFragment() {
    }

    public static VideoThumbnailFragment newInstance() {
        VideoThumbnailFragment fragment = new VideoThumbnailFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        videoEditActivity = (VideoEditActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_new_video_thumbnail, container, false);
        mHolderTopView = ((SeekBar) rootView.findViewById(R.id.thumbnail_handlerTop));
        mTimeLineView = ((CustomTimeLineView) rootView.findViewById(R.id.thumbnail_timeLineView));

        setup();

        return rootView;
    }

    public void reDrawTimeline()
    {
        mTimeLineView.setVideo(Uri.parse(videoEditActivity.videoEditModel.getVideoUri()), videoEditActivity.videoEditModel.getVideoStart(), videoEditActivity.videoEditModel.getVideoEnd());
        mTimeLineView.invalidate();
        System.out.println("masuk redraw");
    }

    private void setup() {
        mTimeLineView.setVideo(Uri.parse(videoEditActivity.videoEditModel.getVideoUri()), videoEditActivity.videoEditModel.getVideoStart(), videoEditActivity.videoEditModel.getVideoEnd());
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (videoEditActivity.mMediaPlayer != null) {
                    System.out.println("masuk hente null");
                    if (videoEditActivity.mMediaPlayer.getDataSource() != null) {
                        setupListener();
                        setupMargin();
                        mHolderTopView.setProgress(0);
                    }
                } else {
                    System.out.println("null euyy");
                }
            }
        }, 1000);
    }

    private void setupListener() {
        mHolderTopView.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                onPlayerIndicatorSeekChanged(progress, fromUser);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                onPlayerIndicatorSeekStart();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                onPlayerIndicatorSeekStop(seekBar);
            }
        });
    }

    private void setupMargin() {
        int widthSeek = mHolderTopView.getThumb().getMinimumWidth() / 2;

        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mHolderTopView.getLayoutParams();
        lp.setMargins(widthSeek - widthSeek, 0, widthSeek - widthSeek, 0);
        mHolderTopView.setLayoutParams(lp);

        lp = (RelativeLayout.LayoutParams) mTimeLineView.getLayoutParams();
        lp.setMargins(widthSeek, 0, widthSeek, 0);
        mTimeLineView.setLayoutParams(lp);

    }

    private void onPlayerIndicatorSeekStart() {
        if (videoEditActivity.mMediaPlayer.isPlaying()) {
            videoEditActivity.mMediaPlayer.pause();
        }
        videoEditActivity.iconPlay.setVisibility(View.GONE);
        mDuration = videoEditActivity.videoEditModel.getVideoDuration();
        mStartDuration = videoEditActivity.videoEditModel.getVideoStart();
        mEndDuration = videoEditActivity.videoEditModel.getVideoEnd();
        videoEditActivity.mMediaPlayer.seekTo(mStartDuration);

    }

    private void onPlayerIndicatorSeekChanged(int progress, boolean fromUser) {
        int duration = ((int) ((mDuration * progress) / 1000L));
        if(fromUser){
            if(duration >= mStartDuration && duration <= mEndDuration){
                videoEditActivity.mMediaPlayer.seekTo(mStartDuration + duration);
            }
        }
    }

    private void onPlayerIndicatorSeekStop(SeekBar seekBar) {
        int duration = ((int) ((mDuration * seekBar.getProgress()) / 1000L));
        System.out.println(String.format("Start : %s | End : %s | Duration : %s | current Dur : %s",
                videoEditActivity.videoEditModel.getVideoStart(),
                videoEditActivity.videoEditModel.getVideoEnd(),
                videoEditActivity.videoEditModel.getVideoDuration(), duration));
        videoEditActivity.mMediaPlayer.seekTo(mStartDuration + duration);
        videoEditActivity.videoEditModel.setVideoThumb(mStartDuration + duration);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
