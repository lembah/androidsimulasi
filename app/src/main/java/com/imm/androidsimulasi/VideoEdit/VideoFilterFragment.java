package com.imm.androidsimulasi.VideoEdit;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.imm.androidsimulasi.R;
import com.sherazkhilji.videffects.AutoFixEffect;
import com.sherazkhilji.videffects.BlackAndWhiteEffect;
import com.sherazkhilji.videffects.BrightnessEffect;
import com.sherazkhilji.videffects.LamoishEffect;
import com.sherazkhilji.videffects.PosterizeEffect;
import com.sherazkhilji.videffects.SepiaEffect;
import com.sherazkhilji.videffects.SharpnessEffect;
import com.sherazkhilji.videffects.TintEffect;
import com.sherazkhilji.videffects.VignetteEffect;


public class VideoFilterFragment extends Fragment {

    private Button btnNormal;
    private Button btnBW;
    private Button btnAutoFix;
    private Button btnSephia;
    private Button btnVignette;
    private Button btnTint;
    private Button btnSharpness;
    private Button btnLamoish;
    private Button btnPosterize;
    private Button btnBrigthness;

    private VideoEditActivity videoEditActivity;

    public VideoFilterFragment() {
    }

    public static VideoFilterFragment newInstance() {
        VideoFilterFragment fragment = new VideoFilterFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        videoEditActivity = (VideoEditActivity)getActivity();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_new_video_filter, container, false);
        btnNormal = (Button) rootview.findViewById(R.id.btn_filter_normal);
        btnBW = (Button) rootview.findViewById(R.id.btn_filter_bw);
        btnAutoFix = (Button) rootview.findViewById(R.id.btn_filter_autoFix);
        btnSephia = (Button) rootview.findViewById(R.id.btn_filter_sephia);
        btnVignette = (Button) rootview.findViewById(R.id.btn_filter_Vignette);
        btnTint = (Button) rootview.findViewById(R.id.btn_filter_Tint);
        btnSharpness = (Button) rootview.findViewById(R.id.btn_filter_sharpness);
        btnLamoish = (Button) rootview.findViewById(R.id.btn_filter_lamoish);
        btnPosterize = (Button) rootview.findViewById(R.id.btn_filter_posterize);
        btnBrigthness = (Button) rootview.findViewById(R.id.btn_filter_Brightness);

        listener();

//        if(videoEditActivity.mMediaPlayer.isPlaying())
//        {
//            videoEditActivity.mMediaPlayer.pause();
//        }
//        videoEditActivity.iconPlay.setVisibility(View.VISIBLE);

        return rootview;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void listener(){

        btnNormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoEditActivity.surfaceView.init(videoEditActivity.mMediaPlayer, null);
//                surfaceView.init(mMediaPlayer, null);
                videoEditActivity.videoEditModel.setVideoFilter(0);
            }
        });

        btnBW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoEditActivity.surfaceView.init(videoEditActivity.mMediaPlayer, new BlackAndWhiteEffect());
//                surfaceView.init(mMediaPlayer, new BlackAndWhiteEffect());
                videoEditActivity.videoEditModel.setVideoFilter(1);
            }
        });

        btnAutoFix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoEditActivity.surfaceView.init(videoEditActivity.mMediaPlayer, new AutoFixEffect(0.1f));
//                surfaceView.init(mMediaPlayer, new AutoFixEffect(0.5f));
                videoEditActivity.videoEditModel.setVideoFilter(2);
            }
        });

        btnSephia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoEditActivity.surfaceView.init(videoEditActivity.mMediaPlayer, new SepiaEffect());
//                surfaceView.init(mMediaPlayer, new SepiaEffect());
                videoEditActivity.videoEditModel.setVideoFilter(3);
            }
        });

        btnVignette.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoEditActivity.surfaceView.init(videoEditActivity.mMediaPlayer, new VignetteEffect(0.5f));
//                surfaceView.init(mMediaPlayer, new VignetteEffect(0.3f));
                videoEditActivity.videoEditModel.setVideoFilter(4);
            }
        });

        btnTint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoEditActivity.surfaceView.init(videoEditActivity.mMediaPlayer, new TintEffect(100));
//                surfaceView.init(mMediaPlayer, new TintEffect(150));
                videoEditActivity.videoEditModel.setVideoFilter(5);
            }
        });

        btnSharpness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoEditActivity.surfaceView.init(videoEditActivity.mMediaPlayer, new SharpnessEffect(0.5f));
//                surfaceView.init(mMediaPlayer, new SharpnessEffect(0.6f));
                videoEditActivity.videoEditModel.setVideoFilter(6);
            }
        });

        btnLamoish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoEditActivity.surfaceView.init(videoEditActivity.mMediaPlayer, new LamoishEffect());
//                surfaceView.init(mMediaPlayer, new LamoishEffect());
                videoEditActivity.videoEditModel.setVideoFilter(7);
            }
        });

        btnPosterize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoEditActivity.surfaceView.init(videoEditActivity.mMediaPlayer, new PosterizeEffect());
//                surfaceView.init(mMediaPlayer, new PosterizeEffect());
                videoEditActivity.videoEditModel.setVideoFilter(8);
            }
        });

        btnBrigthness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoEditActivity.surfaceView.init(videoEditActivity.mMediaPlayer, new BrightnessEffect(0.5f));
//                surfaceView.init(mMediaPlayer, new BrightnessEffect(0.5f));
                videoEditActivity.videoEditModel.setVideoFilter(9);
            }
        });
    }
}
