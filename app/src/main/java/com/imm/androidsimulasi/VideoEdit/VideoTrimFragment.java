package com.imm.androidsimulasi.VideoEdit;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.imm.androidsimulasi.R;

import java.util.ArrayList;
import java.util.List;


import com.imm.androidsimulasi.util.OnProgressVideoListener;
import com.imm.androidsimulasi.util.OnRangeSeekBarListener;
import com.imm.androidsimulasi.util.CustomProgressBarView;
import com.imm.androidsimulasi.util.CustomRangeSeekBarView;
import com.imm.androidsimulasi.util.CustomThumb;
import com.imm.androidsimulasi.util.CustomTimeLineView;

import static com.imm.androidsimulasi.util.StringToTime.stringForTime;

public class VideoTrimFragment extends Fragment {

    private VideoEditActivity videoEditActivity;
    private static final int SHOW_PROGRESS = 2;

    private SeekBar mHolderTopView;
    public CustomRangeSeekBarView mRangeSeekBarView;
    private TextView mTextSize;
    private TextView mTextTimeFrame;
    private TextView mTextTime;
    private CustomTimeLineView mTimeLineView;

    private CustomProgressBarView mVideoProgressIndicator;
    private List<OnProgressVideoListener> mListeners;


    private int mMaxDuration = 0;
    private int mDuration = 0;
    public int mTimeVideo = 0;
    public int mStartPosition = 0;
    public int mEndPosition = 0;

    public VideoTrimFragment() {
    }

    public static VideoTrimFragment newInstance() {
        VideoTrimFragment fragment = new VideoTrimFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        videoEditActivity = (VideoEditActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_new_video_trim, container, false);

        mHolderTopView = ((SeekBar) rootView.findViewById(R.id.handlerTop_video_trim));
        mVideoProgressIndicator = ((CustomProgressBarView) rootView.findViewById(R.id.timeVideoView_video_trim));
        mRangeSeekBarView = ((CustomRangeSeekBarView) rootView.findViewById(R.id.timeLineBar_video_trim));
        mTextSize = ((TextView) rootView.findViewById(R.id.textSize_video_trim));
        mTextTimeFrame = ((TextView) rootView.findViewById(R.id.textTimeSelection_video_trim));
        mTextTime = ((TextView) rootView.findViewById(R.id.textTime_video_trim));
        mTimeLineView = ((CustomTimeLineView) rootView.findViewById(R.id.timeLineView_video_trim));

        setup(Uri.parse(videoEditActivity.videoEditModel.getVideoUri()));

        return rootView;
    }

    private void setupListener() {


        mListeners = new ArrayList<>();
        mListeners.add(new OnProgressVideoListener() {
            @Override
            public void updateProgress(int time, int max, float scale) {
                updateVideoProgress(time);
            }
        });
        mListeners.add(mVideoProgressIndicator);

        mRangeSeekBarView.addOnRangeSeekBarListener(new OnRangeSeekBarListener() {
            @Override
            public void onCreate(CustomRangeSeekBarView rangeSeekBarView, int index, float value) {
                // Do nothing
            }

            @Override
            public void onSeek(CustomRangeSeekBarView rangeSeekBarView, int index, float value) {
                onSeekThumbs(index, value);
            }

            @Override
            public void onSeekStart(CustomRangeSeekBarView rangeSeekBarView, int index, float value) {
                // Do nothing
                if(videoEditActivity.mMediaPlayer.isPlaying())
                {
                    videoEditActivity.mMediaPlayer.pause();
                    videoEditActivity.iconPlay.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onSeekStop(CustomRangeSeekBarView rangeSeekBarView, int index, float value) {
                onStopSeekThumbs(index, value);
            }
        });
        mRangeSeekBarView.addOnRangeSeekBarListener(mVideoProgressIndicator);

        mHolderTopView.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                onPlayerIndicatorSeekChanged(progress, fromUser);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                onPlayerIndicatorSeekStart();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                onPlayerIndicatorSeekStop(seekBar);
            }
        });

        videoEditActivity.mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                return false;
            }
        });
    }

    private void setupMargin() {
        int marge = mRangeSeekBarView.getThumbs().get(0).getWidthBitmap();
        int widthSeek = mHolderTopView.getThumb().getMinimumWidth() / 2;

        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mHolderTopView.getLayoutParams();
        lp.setMargins(marge - widthSeek, 0, marge - widthSeek, 0);
        mHolderTopView.setLayoutParams(lp);

        lp = (RelativeLayout.LayoutParams) mTimeLineView.getLayoutParams();
        lp.setMargins(marge, 0, marge, 0);
        mTimeLineView.setLayoutParams(lp);

        lp = (RelativeLayout.LayoutParams) mVideoProgressIndicator.getLayoutParams();
        lp.setMargins(marge, 0, marge, 0);
        mVideoProgressIndicator.setLayoutParams(lp);
    }

    private void onPlayerIndicatorSeekChanged(int progress, boolean fromUser) {

        int duration = (int) ((mDuration * progress) / 1000L);

        if (fromUser) {
            if (duration < mStartPosition) {
                setProgressBarPosition(mStartPosition);
                videoEditActivity.mMediaPlayer.seekTo(duration);
                duration = mStartPosition;
            } else if (duration > mEndPosition) {
                setProgressBarPosition(mEndPosition);
                duration = mEndPosition;
            }
            setTimeVideo(duration);
        }
        notifyProgressUpdate(false);
    }

    private void onPlayerIndicatorSeekStart() {
        videoEditActivity.callMessageHanler();
        if (videoEditActivity.mMediaPlayer.isPlaying()) {
            videoEditActivity.mMediaPlayer.pause();
            videoEditActivity.iconPlay.setVisibility(View.VISIBLE);
        }
        notifyProgressUpdate(false);
    }

    private void onPlayerIndicatorSeekStop(@NonNull SeekBar seekBar) {
        videoEditActivity.callMessageHanler();
        if (videoEditActivity.mMediaPlayer.isPlaying()) {
            videoEditActivity.mMediaPlayer.pause();
            videoEditActivity.iconPlay.setVisibility(View.VISIBLE);
        }
        int duration = (int) ((mDuration * seekBar.getProgress()) / 1000L);
        videoEditActivity.mMediaPlayer.seekTo(duration);
        setTimeVideo(duration);
        notifyProgressUpdate(false);
    }

    private void setSeekBarPosition() {

        if (mDuration >= mMaxDuration) {
            mStartPosition = mDuration / 2 - mMaxDuration / 2;
            mEndPosition = mDuration / 2 + mMaxDuration / 2;

            mRangeSeekBarView.setThumbValue(0, (mStartPosition * 100) / mDuration);
            mRangeSeekBarView.setThumbValue(1, (mEndPosition * 100) / mDuration);

        } else {
            mMaxDuration = mDuration;
            mStartPosition = videoEditActivity.videoEditModel.getVideoStart();
            mEndPosition = videoEditActivity.videoEditModel.getVideoEnd();
//            mStartPosition = 0;
//            mEndPosition = mDuration;
        }

        if(videoEditActivity.videoEditModel.getVideoCurPos() > 0){
            setProgressBarPosition(videoEditActivity.videoEditModel.getVideoCurPos());
            videoEditActivity.mMediaPlayer.seekTo(videoEditActivity.videoEditModel.getVideoCurPos());
        }else{
            setProgressBarPosition(mStartPosition);
            videoEditActivity.mMediaPlayer.seekTo(mStartPosition);
        }


        mTimeVideo = mDuration;
        mRangeSeekBarView.initMaxWidth();
    }

    private void setTimeFrames() {
        String seconds = getContext().getString(R.string.short_seconds);
        mTextTimeFrame.setText(String.format("%s %s - %s %s", stringForTime(mStartPosition), seconds, stringForTime(mEndPosition), seconds));
        setTimeLong();
    }

    private void setTimeVideo(int position) {
        String seconds = getContext().getString(R.string.short_seconds);
        mTextTime.setText(String.format("%s %s", stringForTime(position), seconds));
    }

    private void setTimeLong() {
        int timeLong = mEndPosition - mStartPosition;
        String seconds = getContext().getString(R.string.short_seconds);
        mTextSize.setText(String.format("%s %s", stringForTime(timeLong), seconds));
    }

    private void onSeekThumbs(int index, float value) {
        int currentSeek = videoEditActivity.mMediaPlayer.getCurrentPosition();
        switch (index) {
            case CustomThumb.LEFT: {
                mStartPosition = (int) ((mDuration * value) / 100L);
                if (currentSeek <= mStartPosition) {
                    videoEditActivity.mMediaPlayer.seekTo(mStartPosition);
                    setProgressBarPosition(mStartPosition);
                }
                break;
            }
            case CustomThumb.RIGHT: {
                mEndPosition = (int) ((mDuration * value) / 100L);
                if (currentSeek > mEndPosition) {
                    videoEditActivity.mMediaPlayer.seekTo(mEndPosition);
                    setProgressBarPosition(mEndPosition);
                }
                break;
            }
        }

        setTimeFrames();
        mTimeVideo = mEndPosition - mStartPosition;

    }

    private void onStopSeekThumbs(int index, float value) {
        videoEditActivity.callMessageHanler();
//        if (videoEditActivity.mMediaPlayer.isPlaying()) {
//            videoEditActivity.mMediaPlayer.pause();
//            videoEditActivity.iconPlay.setVisibility(View.VISIBLE);
//        }

        videoEditActivity.videoEditModel.setVideoStart(mStartPosition);
        videoEditActivity.videoEditModel.setVideoEnd(mEndPosition);
        videoEditActivity.videoEditModel.setVideoDuration(mTimeVideo);
        if(videoEditActivity.videoEditModel.getVideoThumb() <= mStartPosition){
            videoEditActivity.videoEditModel.setVideoThumb(mStartPosition);
        }
        if(videoEditActivity.videoEditModel.getVideoCurPos() <= mStartPosition){
            videoEditActivity.videoEditModel.setVideoCurPos(mStartPosition);
        }
        System.out.println(String.format("Start : %s | End : %s | Duration : %s",
                videoEditActivity.videoEditModel.getVideoStart(),
                videoEditActivity.videoEditModel.getVideoEnd(),
                videoEditActivity.videoEditModel.getVideoDuration()));


    }

    private void updateVideoProgress(int time) {
        if (videoEditActivity.mMediaPlayer == null) {
            return;
        }

        if (time >= mEndPosition) {
            videoEditActivity.callMessageHanler();
            if (videoEditActivity.mMediaPlayer.isPlaying()) {
                videoEditActivity.mMediaPlayer.pause();
                videoEditActivity.iconPlay.setVisibility(View.VISIBLE);
            }
            videoEditActivity.videoEditModel.setResetSeekBar(true);
            return;
        }

        if (mHolderTopView != null) {
            // use long to avoid overflow
            setProgressBarPosition(time);
        }
        videoEditActivity.videoEditModel.setVideoCurPos(time);
        setTimeVideo(time);
    }


    private void setup(Uri uri) {
        mTimeLineView.setVideo(uri, 0 , 0);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (videoEditActivity.mMediaPlayer != null) {
                    System.out.println("masuk hente null");
                    if (videoEditActivity.mMediaPlayer.getDataSource() != null) {

                        setupListener();
                        setupMargin();

                        mDuration = videoEditActivity.videoEditModel.getVideoDuration();
                        mMaxDuration = videoEditActivity.mMediaPlayer.getDuration();

                        setSeekBarPosition();

                        setTimeFrames();
                        setTimeVideo(0);


                    }
                } else {
                    System.out.println("null euyy");
                }
            }
        }, 1000);


    }

    private void setProgressBarPosition(int position) {
        if (mDuration > 0) {
            long pos = 1000L * position / mDuration;
            mHolderTopView.setProgress((int) pos);
        }
    }

    public void notifyProgressUpdate(boolean all) {
        if (mDuration == 0) return;

        int position = videoEditActivity.mMediaPlayer.getCurrentPosition();

        if (all) {
            for (OnProgressVideoListener item : mListeners) {
                item.updateProgress(position, mDuration, ((position * 100) / mDuration));
            }
        } else {
            mListeners.get(1).updateProgress(position, mDuration, ((position * 100) / mDuration));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
