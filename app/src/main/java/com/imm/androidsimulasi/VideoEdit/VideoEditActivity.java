package com.imm.androidsimulasi.VideoEdit;

import android.content.Intent;
import android.content.res.Resources;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import android.widget.ImageView;

import com.imm.androidsimulasi.R;
import com.imm.androidsimulasi.activity.UploadVideoActivity;
import com.imm.androidsimulasi.util.CustomMediaPlayer;
import com.imm.androidsimulasi.util.CustomVideoSurfaceView;
import com.imm.androidsimulasi.util.CustomViewPager;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class VideoEditActivity extends AppCompatActivity {

    private static final int SHOW_PROGRESS = 2;

    private CustomViewPager viewPager;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private String videoUriString;
    public VideoEditModel videoEditModel;

    public VideoEditActivity.ViewPagerAdapter adapter;

    public ImageView iconPlay;
    protected Resources resources;
    public CustomVideoSurfaceView surfaceView = null;
    public CustomMediaPlayer mMediaPlayer = null;

    private VideoTrimFragment videoTrimFragment;
    private VideoThumbnailFragment videoThumbFragment;

    public final VideoEditActivity.MessageHandler mMessageHandler = new VideoEditActivity.MessageHandler(this);

    private int actTab = 0;
    private int lasTab = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_video_edit);
        videoEditModel = new VideoEditModel();
        videoUriString = getIntent().getStringExtra("videoUri");
        videoEditModel.setVideoUri(videoUriString);

        iconPlay = (ImageView) findViewById(R.id.icon_video_play);
        surfaceView = (CustomVideoSurfaceView) findViewById(R.id.surface_view_filter);

        resources = getResources();
        mMediaPlayer = new CustomMediaPlayer();

        try {
            File file = new File(Uri.parse(videoUriString).getPath());
            mMediaPlayer.setDataSource(file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        listener();

        videoTrimFragment = new VideoTrimFragment();
        videoThumbFragment = new VideoThumbnailFragment();

        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(Uri.parse(videoUriString).getPath());
        String height = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT);
        String width = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);
        String rotation = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION);
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long timeInMiliScond = Long.parseLong(time);
        int duration = (int) timeInMiliScond;
        System.out.println(String.format("%s %s %s %s", height, width, rotation, videoUriString));
        surfaceView.setDimension(Integer.parseInt(width), Integer.parseInt(height), Integer.parseInt(rotation));
        surfaceView.init(mMediaPlayer, null);

        videoEditModel.setVideoStart(0);
        videoEditModel.setVideoEnd(duration);
        videoEditModel.setVideoDuration(duration);


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewPager = (CustomViewPager) findViewById(R.id.container);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                actTab = tab.getPosition();
                if (actTab == 2) {
                    videoThumbFragment.reDrawTimeline();
                    if (mMediaPlayer.isPlaying()) {
                        mMediaPlayer.pause();
                    }
                    iconPlay.setVisibility(View.GONE);
                    mMediaPlayer.seekTo(videoEditModel.getVideoThumb());
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                lasTab = tab.getPosition();
                System.out.println(String.format("lastab : %s", lasTab));
                if(lasTab == 2){
                    if (mMediaPlayer.isPlaying()) {
                        mMediaPlayer.pause();
                    }
                    System.out.println(String.format("current seek = %s", videoEditModel.getVideoCurPos()));
                    mMediaPlayer.seekTo(videoEditModel.getVideoCurPos());
                    iconPlay.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
//                System.out.println(tab.getPosition());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        surfaceView.onResume();
    }

    private void listener() {

        final onTapVideo onTapVideo = new onTapVideo();

        surfaceView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, @NonNull MotionEvent motionEvent) {
                onTapVideo.onSingleTapConfirmed(motionEvent);
                return false;
            }
        });

        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.seekTo(1);
                iconPlay.setVisibility(View.VISIBLE);
            }
        });

        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                iconPlay.setVisibility(View.VISIBLE);
            }
        });
    }

    private void onClickVideoPlayPause() {
        if (actTab == 0 || actTab == 1) {
            if (mMediaPlayer.isPlaying()) {
                mMessageHandler.removeMessages(SHOW_PROGRESS);
                iconPlay.setVisibility(View.VISIBLE);
                mMediaPlayer.pause();
            } else {
                if (videoEditModel.getResetSeekbar() == true) {
                    System.out.println("masuk reset");
                    videoEditModel.setResetSeekBar(false);
                    mMediaPlayer.seekTo(videoEditModel.getVideoStart());
                }
                mMessageHandler.sendEmptyMessage(SHOW_PROGRESS);
                iconPlay.setVisibility(View.GONE);
                mMediaPlayer.start();
            }
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new VideoEditActivity.ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new VideoFilterFragment(), "FILTER");
        adapter.addFragment(videoTrimFragment, "TRIM VIDEO");
        adapter.addFragment(videoThumbFragment, "THUMBNAIL");

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.video_editor_menu, menu);
        toolbar.setTitle("Video Edit");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMediaPlayer.stop();
                finish();
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.video_editor_next) {
            Log.d("video uri", videoEditModel.getVideoUri());
            Log.d("video start", String.valueOf(videoEditModel.getVideoStart()));
            Log.d("video end", String.valueOf(videoEditModel.getVideoEnd()));
            Log.d("video thumb", String.valueOf(videoEditModel.getVideoThumb()));
            Log.d("video filter", String.valueOf(videoEditModel.getVideoFilter()));

            Intent i = new Intent(VideoEditActivity.this, UploadVideoActivity.class);
            i.putExtra("videoUri",videoUriString);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void callMessageHanler() {
        mMessageHandler.removeMessages(SHOW_PROGRESS);
    }

//    public void test(){
//        System.out.println("masuk dimati euyy");
//    }

    public static class MessageHandler extends Handler {

        @NonNull
        private final WeakReference<VideoEditActivity> mView;

        MessageHandler(VideoEditActivity view) {
            mView = new WeakReference<>(view);
        }

        @Override
        public void handleMessage(Message msg) {
            VideoEditActivity view = mView.get();
            if (view == null || view.mMediaPlayer == null) {
                return;
            }

            view.videoTrimFragment.notifyProgressUpdate(true);
            if (view.mMediaPlayer.isPlaying()) {
                sendEmptyMessageDelayed(0, 10);
            }
        }
    }

    private class onTapVideo extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            onClickVideoPlayPause();
            return true;
        }
    }

}
