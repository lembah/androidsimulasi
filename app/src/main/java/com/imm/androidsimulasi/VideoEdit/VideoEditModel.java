package com.imm.androidsimulasi.VideoEdit;

/**
 * Created by lembah8 on 12/27/16.
 */

public class VideoEditModel {

    String videoUri;
    int videoFilter;
    int videoStart;
    int videoEnd;
    int videoCurPos;
    int videoThumb;
    int videoDuration;
    boolean resetSeekBar = false;

    public VideoEditModel(){

    }

    public void setVideoUri(String videoUri) {
        this.videoUri = videoUri;
    }

    public void setVideoFilter(int filter) {
        this.videoFilter = filter;
    }

    public void setVideoStart(int startPos) {
        this.videoStart = startPos;
    }

    public void setVideoEnd(int endPos) {
        this.videoEnd = endPos;
    }

    public void setVideoCurPos(int curPos) {
        this.videoCurPos = curPos;
    }

    public void setVideoThumb(int thumbPos) {
        this.videoThumb = thumbPos;
    }

    public void setVideoDuration(int duration) {
        this.videoDuration = duration;
    }

    public void setResetSeekBar(boolean reset){
        this.resetSeekBar = reset;
    }

    public String getVideoUri() {
        return videoUri;
    }

    public int getVideoFilter() {
        return videoFilter;
    }

    public int getVideoStart() {
        return videoStart;
    }

    public int getVideoEnd() {
        return videoEnd;
    }

    public int getVideoCurPos() {
        return videoCurPos;
    }

    public int getVideoThumb() {
        return videoThumb;
    }

    public int getVideoDuration() {
        return videoDuration;
    }

    public boolean getResetSeekbar(){
        return resetSeekBar;
    }
}
