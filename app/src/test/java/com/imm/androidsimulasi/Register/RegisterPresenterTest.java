package com.imm.androidsimulasi.Register;

import com.imm.androidsimulasi.register.RegisterContract;
import com.imm.androidsimulasi.register.RegisterPresenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * Created by lembah8 on 11/30/16.
 */
public class RegisterPresenterTest {
    @Mock
    private RegisterContract.view registerActivity;

    private RegisterPresenter registerPresenter;
    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        registerPresenter = new RegisterPresenter(registerActivity);
    }

    /**
     * Merupakan fungsi untuk memeriksa method Register pada registerPresenter
     * dengan kondisi, semua kondisi terpenuhi sehingga mampu memanggil method doRegister
     */
    @Test
    public void testRegisterTrue(){
        Mockito.when(registerActivity.checkFullname()).thenReturn(true);
        Mockito.when(registerActivity.checkEmail()).thenReturn(true);
        Mockito.when(registerActivity.checkPassword()).thenReturn(true);
        Mockito.when(registerActivity.checkRePassword()).thenReturn(true);
        registerPresenter.Register();
        Mockito.verify(registerActivity).checkFullname();
        Mockito.verify(registerActivity).checkEmail();
        Mockito.verify(registerActivity).checkPassword();
        Mockito.verify(registerActivity).checkRePassword();
        Mockito.verify(registerActivity).doRegister();
    }

    /**
     * Merupakan fungsi untuk memeriksa method Register pada registerPresenter
     * dengan kondisi, kondisi tidak terpenuhi sehingga tidak mungkin memanggil method doRegister
     */
    @Test
    public void testRegisterFalse(){
        Mockito.when(registerActivity.checkFullname()).thenReturn(true);
        Mockito.when(registerActivity.checkEmail()).thenReturn(true);
        Mockito.when(registerActivity.checkPassword()).thenReturn(true);
        Mockito.when(registerActivity.checkRePassword()).thenReturn(false);
        registerPresenter.Register();
        Mockito.verify(registerActivity).checkFullname();
        Mockito.verify(registerActivity).checkEmail();
        Mockito.verify(registerActivity).checkPassword();
        Mockito.verify(registerActivity).checkRePassword();
        Mockito.verify(registerActivity, Mockito.never()).doRegister();
    }
}
