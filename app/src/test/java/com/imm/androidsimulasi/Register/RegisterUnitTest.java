package com.imm.androidsimulasi.Register;

/**
 * Created by lembah8 on 11/30/16.
 */

import com.imm.androidsimulasi.register.RegisterActivity;
import com.imm.androidsimulasi.register.RegisterContract;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RegisterUnitTest {
    private RegisterContract.view view;
    private static String fullname = "rahmat zulfikri";
    private static String email = "zulfikri@gmail.com";
    private static String password = "password";

    @Before
    public void setup(){
        view = new RegisterActivity();
    }

    @Test
    public void isEmpty_false()
    {
        boolean test = view.isEmpty(fullname);
        assertEquals(test,false);
    }

    @Test
    public void isEmpty_true(){
        boolean test = view.isEmpty("");
        assertEquals(test,true);
    }

    @Test
    public void isEmailValid_true(){
        assertEquals(view.isEmailValid(email),true);

    }

    @Test
    public void isEmailValid_false(){
        assertEquals(view.isEmailValid(fullname),false);
    }

    @Test
    public void isPasswordValid_true(){
        assertEquals(view.isPasswordValid(password),false);
    }

    @Test
    public void isPasswordValid_false(){
        assertEquals(view.isPasswordValid("pass"),true);
    }

    @Test
    public void isRePasswordValid_true(){
        assertEquals(view.isRePasswordValid(password, password),true);
    }

    @Test
    public void isRePasswordValid_false(){
        assertEquals(view.isRePasswordValid(password, "pass"),false);
    }
}
